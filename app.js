const express = require('express');
const path = require('path');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const compression = require('compression');
const keys = require('./config/keys');

/** Connect to DB */

mongoose.connect(keys.MONGO_URI, { useNewUrlParser: true })
  .then(() => console.log('Database connected'))
  .catch(err => console.log(err))


/** Require routes */

const authRoute = require('./routes/api/auth');
const hotelRoute = require('./routes/api/hotel');
const reserveRoute = require('./routes/api/reserve');

/** Connect middlewares */

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

/** Connect routes */

app.use('/api/auth', authRoute);
app.use('/api/hotel', hotelRoute);
app.use('/api/reserve', reserveRoute);

app.use(compression());

if(process.env.NODE_ENV === 'production') { 
  app.use(express.static('client/build'));

  app.get('*', (req, res) => {
    res.sendFile(
      path.resolve(
        __dirname, 'client', 'build', 'index.html'
      )
    )
  })
}

module.exports = app;