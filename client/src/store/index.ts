import createSagaMiddleware  from 'redux-saga';
import logger from 'redux-logger';
import { applyMiddleware, createStore } from 'redux';
import { routerMiddleware } from 'react-router-redux';

// Local imports

import reducer from './reducer';
import history from '../history';

import { localStorageMiddleware } from '../middlewares';

import { saga as rootSaga } from './saga';

const customRouterMiddleware = routerMiddleware(history);

const sagaMiddleware = createSagaMiddleware();

const enhancer: any = () => {
  if (process.env.NODE_ENV === 'production') {
    return applyMiddleware(sagaMiddleware, customRouterMiddleware, localStorageMiddleware);
  } else {
    return applyMiddleware(sagaMiddleware, customRouterMiddleware, localStorageMiddleware, logger);
  }
};

const store = createStore(reducer, enhancer());
sagaMiddleware.run(rootSaga);

export default store;