import { all } from 'redux-saga/effects';
import { saga as  authSaga } from 'src/ducks/auth';
import { saga as  hotelsSaga } from 'src/ducks/hotels';
import { saga as  hotelSaga } from 'src/ducks/hotel';
import { saga as  searchSaga } from 'src/ducks/search';
import { saga as  footerSaga } from 'src/ducks/footer';
import { saga as  cabinetSaga } from 'src/ducks/cabinet';

export const saga = function * () {
  yield all([
    authSaga(),
    hotelsSaga(),
    hotelSaga(),
    searchSaga(),
    footerSaga(),
    cabinetSaga()
  ])
};