import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';

// Local

import * as ErrorsDuck from '../ducks/errors';
import * as AuthDuck from '../ducks/auth';
import * as HotelsDuck from '../ducks/hotels';
import * as HotelDuck from '../ducks/hotel';
import * as SearchDuck from '../ducks/search';
import * as FooterDuck from '../ducks/footer';
import * as CabinetDuck from '../ducks/cabinet';

export default combineReducers({
  router,
  [AuthDuck.moduleName]: AuthDuck.reducer,
  [HotelsDuck.moduleName]: HotelsDuck.reducer,
  [HotelDuck.moduleName]: HotelDuck.reducer,
  [SearchDuck.moduleName]: SearchDuck.reducer,
  [ErrorsDuck.moduleName]: ErrorsDuck.reducer,
  [FooterDuck.moduleName]: FooterDuck.reducer,
  [CabinetDuck.moduleName]: CabinetDuck.reducer,
  form: formReducer
})