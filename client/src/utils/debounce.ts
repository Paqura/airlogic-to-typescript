// @ts-ignore
import debounce from 'lodash.debounce'; 

export const debounceFn = (fn: any, args: any) => {
  return debounce(fn.bind(null, args), 20);
}; 