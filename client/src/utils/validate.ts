import * as validator from 'validator';

export interface IValidate {
  name?: string;
  email?: string;
  password?: string;
  submitState?: boolean;
};

export default (values: IValidate) => {
  const errors: any = {};
  
  if(!values.name) {
    errors.name = 'Field is required';
  }
  
  if(!values.email) {
    errors.email = 'Email field is required';
  }
  if(values.email && !validator.isEmail(values.email)) {
    errors.email = 'Email not valid. Example: hello@gmail.com';
  }

  if(!values.password) {
    errors.password = 'Password is required';
  }

  return errors;
}