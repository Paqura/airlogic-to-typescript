import axios from 'axios';
import {Record} from 'immutable';
import { put, call, all, takeEvery } from 'redux-saga/effects';

// API

import { HOTEL, RESERVE } from 'src/api';
// LOCAL IMPORTS

import { IReserveHotelAction } from '../models/ducks/hotel';
import { APPEND_ERROR_MESSAGE } from './errors';

export const moduleName = 'hotel';

// SELECTORS

// TYPES

export const FETCH_ONE_HOTEL_REQUEST = 'FETCH_ONE_HOTEL_REQUEST';
export const FETCH_ONE_HOTEL_SUCCESS = 'FETCH_ONE_HOTEL_SUCCESS';
export const FETCH_ONE_HOTEL_FAILURE = 'FETCH_ONE_HOTEL_FAILURE';

export const RESERVE_HOTEL_REQUEST = 'RESERVE_HOTEL_REQUEST';
export const RESERVE_HOTEL_SUCCESS = 'RESERVE_HOTEL_SUCCESS';
export const RESERVE_HOTEL_FAILURE = 'RESERVE_HOTEL_FAILURE';

export const CLEAR_HOTEL_STATE = 'CLEAR_HOTEL_STATE';

// ACTIONS CREATURES

export const fetchOneHotel = (id: string) => {
  return {
    type: FETCH_ONE_HOTEL_REQUEST,
    payload: {
      id
    }
  }
};

export const clearHotelState = () => {
  return {
    type: CLEAR_HOTEL_STATE
  }
};

export const reserveHotel = (payload: any) => {
  return {
    type: RESERVE_HOTEL_REQUEST,
    payload 
  }
};

export const reserveHotelWithoutAuth = () => {
  return {
    type: APPEND_ERROR_MESSAGE,
    payload: 'Сначала зарегистрируйтесь'
  }
}

export const reserveHotelSucess = () => {
  return {
    type: APPEND_ERROR_MESSAGE,
    payload: 'Отель добавлен в заказы. Чтобы просмотреть - перейдите в кабинет'
  }
}

// SAGA

export const fetchOneHotelSaga = function* (action: IReserveHotelAction) {  
  const hotelID = action.payload.id;
  
  const response = yield call(axios.get, `${HOTEL.CURRENT}${hotelID}`);
  const isReserve = yield call(axios.get, `${RESERVE.DEFAULT}${hotelID}`);

  try {
    const { data } = response;
    const payload = {
      ...data,
      isReserve: isReserve.data
    };

    yield put({
      type: FETCH_ONE_HOTEL_SUCCESS,
      payload
    })

  } catch (error) {
    yield put({
      type: FETCH_ONE_HOTEL_FAILURE,
      payload: error.response.data.message
    })
  }
};

export const reserveHotelSaga = function* (action: IReserveHotelAction) {
  const {payload} = action; 

  /** TODO сделать проверку на бронь
   *  и на соответствие даты или дату на клиенте проверять, 
   *  точнее в компоненнте
   */
  
  try {
    const isSend = yield call(axios.post, RESERVE.DEFAULT, payload);

    yield put({
      type: RESERVE_HOTEL_SUCCESS,
      payload: isSend.data.message
    });

  } catch (error) {
    const {response} = error;
    yield put({
      type: RESERVE_HOTEL_FAILURE, 
      payload: response.data.message
    });    
  }
};

export const saga = function* () {
  yield all([
    takeEvery(FETCH_ONE_HOTEL_REQUEST, fetchOneHotelSaga),
    takeEvery(RESERVE_HOTEL_REQUEST, reserveHotelSaga)    
  ])
};

// REDUCER

const HotelSchema = Record({
  isLoading: false,
  info: null,
  errors: null,
  reserveState: null
});

export const reducer = (state = new HotelSchema(), action: any) => {
  const { type, payload } = action;

  switch(type) {
    case FETCH_ONE_HOTEL_REQUEST:
      return state
        .set('isLoading', true);
    case FETCH_ONE_HOTEL_SUCCESS:
      return state
        .set('info', payload)
        .set('isLoading', false);

    case RESERVE_HOTEL_REQUEST:
      return state
        .set('isLoading', true);
    case RESERVE_HOTEL_SUCCESS:
      return state
        .set('errors', null)
        .set('reserveState', payload)
        .set('isLoading', false) 
    case RESERVE_HOTEL_FAILURE: 
      return state
        .set('errors', payload)
        .set('isLoading', false)

    case CLEAR_HOTEL_STATE:
      return state
        .set('info', null)

    default: 
      return state;
  }
};
