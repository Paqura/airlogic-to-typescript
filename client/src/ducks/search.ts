import axios from 'axios';
import { put, call, all, takeEvery } from 'redux-saga/effects';
import { Record } from 'immutable';

// API

import { HOTEL } from 'src/api';

export const moduleName = 'search';

// TYPES

export const FIND_HOTELS_REQUEST = 'FIND_HOTELS_REQUEST';
export const FIND_HOTELS_SUCCESS = 'FIND_HOTELS_SUCCESS';
export const FIND_HOTELS_FAILURE = 'FIND_HOTELS_FAILURE';

// ACTION CREATORS

export const findHotelsRequest = (value: any, history: any) => {  
  return {
    type: FIND_HOTELS_REQUEST,
    payload: {value, history}
  }
}

// SAGAS

export const findSaga = function* (action:any) {
  const { value, history } = action.payload;
  try {
    const { data } = yield call(axios.post, HOTEL.CURRENT, value); 

    yield put({
      type: FIND_HOTELS_SUCCESS,
      payload: data
    })   

  } catch (error) {
    yield put({
      type: FIND_HOTELS_FAILURE,
      payload: error.response.data.message
    })
  }
  
  if(history.location.pathname === '/search') {
    return;
  }
  
  history.push('/search');
}

export const saga = function* () {
  yield all([
    takeEvery(FIND_HOTELS_REQUEST, findSaga)
  ])
};


const HotelsSchema = Record({
  errors: null,
  resultList: [],
  isLoading: false
});

export const reducer = (state: any = new HotelsSchema(), action: any) => {
  const { type, payload } = action;
  switch(type) {    
    case FIND_HOTELS_REQUEST:
      return state
        .set('isLoading', true);
        
    case FIND_HOTELS_SUCCESS:
      return state
        .set('resultList', payload)
        .set('errors', null)
        .set('isLoading', false);   
        
    case FIND_HOTELS_FAILURE:
      return state
        .set('resultList', [])
        .set('errors', payload)
        .set('isLoading', false);
          
    default:
      return state;  
  }
}