import { put, all, call, takeEvery, select } from 'redux-saga/effects';
import { Record } from 'immutable';
import axios from 'axios';

// API

import { HOTELS } from 'src/api';

// LOCALS IMPORTS

import { RESET_STATE_OF_ERROR } from './errors';

// CODE

export const moduleName: string = 'hotels';

// SELECTORS

export const getHotelList = (state: any) => state.get('hotels');

export const getPayload = (list: any, payload: any) => {
  const id2 = payload.map((it: any) => it._id);
  return list.filter((element: any) => id2.includes(element._id));
};

export const getFilteredList = (list: any, payload: any) => {
  const id2 = payload.map((it: any) => it._id);
  return list.filter((element: any) => id2.includes(element._id));
};

// TYPES

export const FETCH_HOTELS_REQUEST = 'FETCH_HOTELS_REQUEST';
export const FETCH_HOTELS_SUCCESS = 'FETCH_HOTELS_SUCCESS';
export const FETCH_HOTELS_FAILURE = 'FETCH_HOTELS_FAILURE';

export const FETCH_LAZY_HOTELS_REQUEST = 'FETCH_LAZY_HOTELS_REQUEST';
export const FETCH_LAZY_HOTELS_SUCCESS = 'FETCH_LAZY_HOTELS_SUCCESS';
export const FETCH_LAZY_HOTELS_FAILURE = 'FETCH_LAZY_HOTELS_FAILURE';

// ACTION CREATORS

export const fetchHotelsRequest = () => {
  return {
    type: FETCH_HOTELS_REQUEST
  }
};

export const fetchLazyHotelsRequest = () => {
  return {
    type: FETCH_LAZY_HOTELS_REQUEST
  }
};

// SAGAS

export const fetchHotelSaga = function* (action: any) {
  const state = yield select();  

  if(state[moduleName].hotels.length > 0) {
    yield put({
      type: FETCH_HOTELS_SUCCESS,
      payload: state[moduleName].hotels
    })
    return;
  }

  const result = yield call(axios.get, HOTELS.FIRST_REQUEST);

  try {    
    const { data } = result;
    yield put({
      type: FETCH_HOTELS_SUCCESS,
      payload: data
    })

  } catch (error) {
    yield put({
      type: FETCH_HOTELS_FAILURE,
      payload: error
    })
  }  
};

export const fetchLazyHotelSaga = function* (action: any) {
  const state = yield select();
  const { offset, limit } = state[moduleName];
  const newOffset = offset + limit; 

  try {
    const result = yield call(axios.get, `/api/hotel/?offset=${newOffset}&limit=8`);
    const { data } = result;
    
    yield put({
      type: FETCH_LAZY_HOTELS_SUCCESS,
      payload: data
    })

  } catch (error) {
    yield put({
      type: FETCH_LAZY_HOTELS_FAILURE,
      payload: error
    })
  }
};



export const saga = function* () {
  yield all([
    takeEvery(FETCH_HOTELS_REQUEST, fetchHotelSaga),
    takeEvery(FETCH_LAZY_HOTELS_REQUEST, fetchLazyHotelSaga)
  ])
};

// REDUCER

const HotelSchema = Record({
  offset: 0,
  limit: 8,
  isLoading: false,
  hotels: [],
  error: null,
  loaded: false,
  lazyLoading: false
});

export const reducer = (state: any = new HotelSchema(), action: any) => {
  const { type, payload } = action;
  switch(type) {
    case FETCH_HOTELS_REQUEST:
      return state
        .set('isLoading', true);
    case FETCH_HOTELS_SUCCESS:  
      const currentState = getPayload(state.hotels, payload);    
      return state
        .set('hotels', currentState.length > 0 ? currentState : payload)
        .set('isLoading', false) 
        .set('error', null);  
    case FETCH_HOTELS_FAILURE:
      return state
        .set('isLoading', false)         
        .set('error', payload);  

    case FETCH_LAZY_HOTELS_REQUEST:
      return state
        .set('lazyLoading', true);
    case FETCH_LAZY_HOTELS_SUCCESS:
      const newOffset = state.offset + state.limit;
      const newPayload = state.hotels.concat(payload);        
      return state
        .set('hotels', newPayload)
        .set('isLoading', false)  
        .set('offset', newOffset)
        .set('lazyLoading', false)
        .set('loaded', Object.keys(payload).length < 8)        
        .set('error', null);

    case FETCH_LAZY_HOTELS_FAILURE:
      return state
        .set('lazyLoading', false);
        
    case RESET_STATE_OF_ERROR:
      return state
        .set('error', null)
          
    default:
      return state;  
  }
}