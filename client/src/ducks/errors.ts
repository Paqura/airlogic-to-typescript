export const moduleName: string = 'errors';

// TYPES

export const RESET_STATE_OF_ERROR = 'RESET_STATE_OF_ERROR';
export const APPEND_ERROR_MESSAGE = 'APPEND_ERROR_MESSAGE';

// ACTION CREATORS

export const resetStateOfErrors = () => {
  return {
    type: RESET_STATE_OF_ERROR
  }
};

// REDUCER

const initialState = {
  message: null
};

export const reducer = (state = initialState, action: any) => {
  const { type, payload } = action;
  switch(type) {
    case APPEND_ERROR_MESSAGE:
      return {
        ...state,
        message: payload
      }

    case RESET_STATE_OF_ERROR:
      return {
        ...state,
        message: null
      }

    default: 
      return state;
  }
}

