import { put, all, takeEvery } from 'redux-saga/effects';
import { Record } from 'immutable';

export const moduleName: string = 'footer';

// TYPES

export const TOGGLE_FOOTER_STATE = 'TOGGLE_FOOTER_STATE';
export const TOGGLE_FOOTER_STATE_CHANGE = 'TOGGLE_FOOTER_STATE_CHANGE';

// ACTION CREATERS

export const toggleFooterState = () => {
  return {
    type: TOGGLE_FOOTER_STATE
  }
};

// SAGAS

export const toggleFooterStateSaga = function* (action: any) {
  yield put({
    type: TOGGLE_FOOTER_STATE_CHANGE    
  })
};

export const saga = function* () {
  yield all([
    takeEvery(TOGGLE_FOOTER_STATE, toggleFooterStateSaga)
  ])
};

// REDUCER

const FooterSchema = Record({
  isVisible: false
});

export const reducer = (state = new FooterSchema(), action: any) => {
  const { type } = action;
  switch(type) {
    case TOGGLE_FOOTER_STATE_CHANGE:
      const currentState = state.get('isVisible');
      return state
        .set('isVisible', !currentState);
        
    default:
      return state;  
  }
}