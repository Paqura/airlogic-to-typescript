import axios from 'axios';
import { put, call, all, takeEvery, select, take } from 'redux-saga/effects';

// LOCAL IMPORTS

import { RESERVE, HOTELS, HOTEL } from 'src/api';
import { IOrder } from 'src/containers/cabinet/02-orders/02-Order';

// MODULENAME

export const moduleName = 'cabinet';

// SELECTORS

export const getHotelsId = (reservers: any) => {
  return reservers.map((reserver: any) => {
    return reserver.hotel;
  });
}

// TYPES

export const FETCH_ORDERS_REQUEST = 'FETCH_ORDERS_REQUEST';
export const FETCH_ORDERS_SUCCESS = 'FETCH_ORDERS_SUCCESS';
export const FETCH_ORDERS_FAILURE = 'FETCH_ORDERS_FAILURE';

export const FETCH_DETAIL_REQUEST = 'FETCH_DETAIL_REQUEST';
export const FETCH_DETAIL_SUCCESS = 'FETCH_DETAIL_SUCCESS';
export const FETCH_DETAIL_FAILURE = 'FETCH_DETAIL_FAILURE';

// ACTION CREATORS

type ICabinetAction = IFetchOrders | IFetchDetail;


interface IFetchOrders {
  type: 'FETCH_ORDERS_REQUEST',
  payload: string;
};

interface IFetchDetail {
  type: 'FETCH_DETAIL_REQUEST',
  payload: string;
};

export const fetchOrders = (userId: string): IFetchOrders => {
  return {
    type: FETCH_ORDERS_REQUEST,
    payload: userId
  }
};

export const fetchDetail = (hotelId: string): IFetchDetail => {
  return {
    type: FETCH_DETAIL_REQUEST,
    payload: hotelId
  }
}

// SAGAS

export const fetchOrdersSaga = function* (action: IFetchOrders) { 
  const {payload} = action;
  try {
    const {data} = yield call(axios.get, `${RESERVE.FIND_IDS}${payload}`);

    yield put({
      type: FETCH_ORDERS_SUCCESS,
      payload: data
    })
  } catch (error) {
    yield put({
      type: FETCH_ORDERS_FAILURE,
      payload: error
    })
  }  
};

export const fetchDetailSaga = function* (action: IFetchDetail) {
  const hotelId = action.payload; 
  try {
    const detail = yield call(axios.get, `${HOTEL.CURRENT}${hotelId}`);

    yield put({
      type: FETCH_DETAIL_SUCCESS,
      payload: detail.data
    })
  } catch (error) {
    yield put({
      type: FETCH_DETAIL_FAILURE,
      payload: error
    })
  }
}

export const saga = function* () {
  yield all([
    takeEvery(FETCH_ORDERS_REQUEST, fetchOrdersSaga),
    takeEvery(FETCH_DETAIL_REQUEST, fetchDetailSaga)
  ])
}

// REDUCER

interface IState {
  orders: IOrder[];
  settings: any;
  isLoading: boolean;
  detail: null | IOrder;
};

const initialState = {
  orders: [],
  settings: {},
  isLoading: false,
  detail: null
};

export const reducer = (state: IState = initialState, action: any): IState => {
  const {type, payload} = action;

  switch(type) {
    case FETCH_ORDERS_REQUEST:
      return {
        ...state,
        isLoading: true
      };
    case FETCH_ORDERS_SUCCESS: 
      return {
        ...state,
        isLoading: false,
        orders: payload
      }  

    case FETCH_DETAIL_REQUEST:
      return {
        ...state,
        detail: null,
        isLoading: true
      }     
    case FETCH_DETAIL_SUCCESS:
      return {
        ...state,
        detail: payload,
        isLoading: false
      }   
    case FETCH_DETAIL_FAILURE:  
      return {
        ...state,
        detail: null,
        isLoading: false
      }
    
    default: return state;
  }
};