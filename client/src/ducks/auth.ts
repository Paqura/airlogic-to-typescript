import { put, all, call, takeEvery } from 'redux-saga/effects';
import { Record } from 'immutable';
import axios from 'axios';
import * as jwt_decode from 'jwt-decode';

// API

import { AUTH } from 'src/api';

// LOCAL IMPORTS

import { RESET_STATE_OF_ERROR, APPEND_ERROR_MESSAGE } from './errors';

import setAuthToken from '../utils/setAuthToken';
import isEmpty from '../utils/isEmpty';

// INTERFACES 

import * as AuthInterface from '../models/ducks/auth';

// CODE BLOCK

export const moduleName: string = 'auth';

// TYPES

export const LOGIN_FORM_REQUEST = 'LOGIN_FORM_REQUEST';
export const LOGIN_FORM_SUCCESS = 'LOGIN_FORM_SUCCESS';
export const LOGIN_FORM_FAILURE = 'LOGIN_FORM_FAILURE';

export const REGISTER_FORM_REQUEST = 'REGISTER_FORM_REQUEST';
export const REGISTER_FORM_SUCCESS = 'REGISTER_FORM_SUCCESS';
export const REGISTER_FORM_FAILURE = 'REGISTER_FORM_FAILURE';

export const CHECK_USER_TOKEN = 'CHECK_USER_TOKEN';
export const LOGOUT = 'LOGOUT';

// ACTION CREATORS

export const checkUserToken = () => {
  return {
    type: CHECK_USER_TOKEN
  }
}

export const loginFormRequest = (values: AuthInterface.ILoginFields, history: any): AuthInterface.IAuthLoginAction => {
  const {email, password} = values;
  
  return {
    type: LOGIN_FORM_REQUEST,
    payload: {
      values: {
        email,
        password
      },
      history
    }
  }
};

export const registerFormRequest = (values: AuthInterface.IRegisterFields, history: any): AuthInterface.IAuthRegisterAction => {
  const {name, email, password} = values;
  return {
    type: REGISTER_FORM_REQUEST,
    payload: {
      values: {
        name,
        email,
        password
      },
      history
    }
  }
};

export const setCurrentUser = (decoded: any) => {
  return {
    type: LOGIN_FORM_SUCCESS,
    payload: decoded
  };
};

export const logoutUser: any = async () => {
  window.localStorage.removeItem('jwtToken');
  // await setAuthToken(false);
  await setCurrentUser({});
};

// SAGAS

export const loginFormRequestSaga = function* (action: AuthInterface.IAuthLoginAction){ 
  const {payload} = action;

  const history = payload.history;

  const params: AuthInterface.ILoginFields = {
    email: payload.values.email,
    password: payload.values.password
  };
  
  try {
    const response = yield call(axios.post, AUTH.LOGIN, params);
    const { token } = response.data;
    const decoded = jwt_decode(token); 
    
    localStorage.setItem("jwtToken", token);
    setAuthToken(token);

    yield put({
      type: LOGIN_FORM_SUCCESS,
      payload: decoded
    });

    history.push('/');

  } catch (error) {
    const errorMessage = error.response.data.message;

    yield put({
      type: LOGIN_FORM_FAILURE
    })

    yield put({
      type: APPEND_ERROR_MESSAGE,
      payload: errorMessage
    })
  }  
};

export const registerFormRequestSaga = function* (action: AuthInterface.IAuthRegisterAction) { 
  const {payload} = action;

  const history = payload.history;

  const params: AuthInterface.IRegisterFields = {
    name: payload.values.name,
    email: payload.values.email,
    password: payload.values.password
  };
  
  try {
    const response = yield call(axios.post, AUTH.REGISTER, params);  
    
    yield put({
      type: REGISTER_FORM_SUCCESS,
      payload: {
        name: response.data.name,
        message: 'Регистрация прошла успешно'
      }
    })

    history.push('/login');

  } catch (error) {
    yield put({
      type: REGISTER_FORM_FAILURE
    })

    yield put({
      type: APPEND_ERROR_MESSAGE,
      payload: error.response.data.message || error
    })
  }
};

export const saga = function* () {
  yield all([
    takeEvery(LOGIN_FORM_REQUEST, loginFormRequestSaga),
    takeEvery(REGISTER_FORM_REQUEST, registerFormRequestSaga),
  ])
};

// REDUCER

const AuthSchema = Record({
  isAuthenticated: false,
  user: {},
  message: null,
  submit: false 
});

export const reducer = (state = new AuthSchema(), action: any) => {
  const { type, payload } = action;
  switch(type) {
    case LOGIN_FORM_REQUEST:
      return state
        .set('submit', true);

    case LOGIN_FORM_SUCCESS: 
      return state
        .set('isAuthenticated', !isEmpty(payload))
        .set('message', null)
        .set('user', payload)
        .set('submit', true);

    case LOGIN_FORM_FAILURE:
      return state
        .set('submit', false);

    case REGISTER_FORM_REQUEST:
      return state
        .set('submit', true);
    case REGISTER_FORM_SUCCESS:
      return state
        .set('submit', true);
    case REGISTER_FORM_FAILURE:
      return state
        .set('submit', false);

    default:
      return state;
  }
}