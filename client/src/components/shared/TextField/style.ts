import styled from 'styled-components';

export const Wrapper = styled.div`  
  margin-bottom: calc(var(--spacer)*1.125);
`;

export const Root = styled.div`
  position: relative;
`;

export const FloatLabel = styled.label`
  position: absolute;  
  margin-bottom: 0;
  bottom: 4px;
  left: 0;
  transition: 200ms;
  will-change: transform;
`;

export const TextField = styled.input`
  border: none;
  border-bottom: 1px solid var(--primary-grey);
  opacity: 0.7;
  padding: calc(var(--spacer)/2) var(--spacer);
  padding-left: calc(var(--spacer)/4);
  width: 100%;  
  
  & + label {
    transform: ${(props: any) => (props.dirty || (props.dirty && props.valid)) ? 'translate(0px, -28px)' : 'none'};
    font-size: ${(props: any) => (props.dirty || (props.dirty && props.valid)) ? '12px' : 'var(--font-size)'};
  }

  &:focus {
    outline: none;
    border-color: var(--primary-color);
  }

  &:focus + label {
    transform: translate(0px, -28px);
    font-size: 12px;
  }
`;

export const TextareaField = styled.textarea`
  border: none;
  border-bottom: 1px solid var(--primary-grey);
  opacity: 0.7;
  padding: calc(var(--spacer)/2) var(--spacer);
  padding-left: calc(var(--spacer)/4);
  width: 100%;  

  &:focus {
    outline: none;
    border-color: var(--primary-color);
  }
`;

export const ErrorText = styled.div`
  color: var(--primary-black);
  font-size: calc(var(--font-size) * 0.875);
  padding: calc(var(--spacer)/4) var(--spacer);
  background-color: rgba(242, 0, 0, 0.06);
  margin-top: 6px;
  letter-spacing: 0.4px;
`;