import * as React from 'react';
import * as FormField from './style';

interface IProps {
  id?: string;
  label?: string;
  name?: string;
  type?: string;
  meta?: any;
  input?: any;
};

export default (props: IProps) => {
  const { id, label, name, type, meta, input } = props;
  return (
    <FormField.Wrapper>
      <FormField.Root>
        <FormField.TextField
          id={id}
          name={name}
          type={type}
          {...input}
          {...meta}
        />
        <FormField.FloatLabel 
          htmlFor={id}
        >
          { label }
        </FormField.FloatLabel>
      </FormField.Root>
      {(meta.error && meta.touched) && (
        <FormField.ErrorText>{meta.error}</FormField.ErrorText>
      )}
    </FormField.Wrapper>   
  )
}