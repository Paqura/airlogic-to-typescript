import * as Loadable from 'react-loadable';

// Local imports

import Loader from '../Loader';

const Footer = Loadable({
  loader: () => import('./Footer'),
  loading: Loader.CircleLoader,
});

export default Footer;  