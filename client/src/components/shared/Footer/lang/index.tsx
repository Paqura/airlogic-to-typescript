import * as React from 'react';

// Local imports

import * as Lang from './style';

export default () => {
  return (
    <div className="lang">
      <Lang.Toggler>
        England
        <Lang.TogglerIcon />
      </Lang.Toggler>
    </div>
  )
};
