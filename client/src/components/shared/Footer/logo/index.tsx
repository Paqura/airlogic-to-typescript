import * as React from 'react';

// Local imports

import * as Logotype from './style';

export default () => {
  return (
    <Logotype.Root>
      <Logotype.Link href="/">Airlogica</Logotype.Link>
    </Logotype.Root>
  )
};
