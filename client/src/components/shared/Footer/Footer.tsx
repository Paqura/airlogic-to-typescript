import * as React from 'react';
import {connect} from 'react-redux';

// Local imports

import { FloatButton } from '../../shared/Button'; 
import * as FLoatFooter from './style';
import Logo from './logo';
import Lang from './lang';
import Currency from './currency';

import * as FooterDuck from 'src/ducks/footer';

class Footer extends React.Component<any, any> {
  render() {
    const { isVisible } = this.props;
    return (
      <React.Fragment>
        <FloatButton 
          isVisible={isVisible}
          action={this.props.toggleFooterState}
        />
        <FLoatFooter.Root isVisible={isVisible}>
          <div className="container">
            <FLoatFooter.List>
              <Logo />
              <Lang />
              <Currency />
            </FLoatFooter.List>
          </div>
        </FLoatFooter.Root>
      </React.Fragment>
    )
  }
};

const mapStateToProps = (state: any) => ({
  isVisible: state[FooterDuck.moduleName].isVisible
});

const { toggleFooterState } = FooterDuck;

const mapDispatchToProps = ({
  toggleFooterState
});

export default connect(mapStateToProps, mapDispatchToProps)(Footer);