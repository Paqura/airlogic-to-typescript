import styled from 'styled-components';
import { Default } from '../../../shared/Button/style'; 

export const Toggler = styled(Default)`  
  display: flex;
  align-items: center;

  &:hover {
    opacity: 0.8;
  }

  &:focus {
    outline: var(--focus-border);
  }
`;

export const TogglerIcon = styled.span`
  display: flex;
  align-items: center;
  margin-left: calc(var(--spacer)/2);
  transform: translateY(12%);
  width: 12px;
  height: 12px;
  background-image: url('../img/down-arrow.svg');
  background-size: 12px 12px;  
`;