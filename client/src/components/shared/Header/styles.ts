import styled from 'styled-components';

export const Root = styled.header`
  position: relative;
  background-color: var(--primary-color);
  background-image: url('./img/main-bg.jpg');
  background-attachment: fixed;
  height: 100vh;
  background-size: cover; 
  @media (max-width: 420px) {
    background-image: none;
    min-height: 100%;
    height: 40vh;
    margin-bottom: 80px;
  }
`; 

export const Nav = styled.nav`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 80px;
  width: 100%;

  @media (max-width: 420px) {
    flex-direction: column;
    height: calc(100% - var(--spacer)*4);
    justify-content: center;    
  }
`; 

// Shared 

export const SharedRoot = styled(Root)`
  height: auto;
  margin-bottom: calc(var(--spacer)*2);
  background-color: transparent;
  box-shadow: var(--card-shadow);

  @media (max-width: 420px) {
    padding: var(--spacer) 0;
    margin-bottom: var(--spacer);
  }
`;

export const SharedNav = styled(Nav)`
  & a {
    color: var(--primary-black) !important;
  }
`;
