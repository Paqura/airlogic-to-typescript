import * as React from 'react';
import  Typing from 'react-typing-animation';

// Local imports

import * as Head from './styles';
import Logo from './logo';
import SearchBar from './search';
import Auth from './auth';

interface IProps {
  isAuthenticated: boolean;
  find: (params: { name: string}, history: History) => void;
};

const MainPageHeader = (props: IProps) => {
  const { isAuthenticated } = props;
  return (
    <Head.Root>
      <div className="container h-100">        
        <Head.Nav>
          <Logo /> 
          <Auth 
            isAuthenticated={isAuthenticated}
          />
        </Head.Nav>
        <SearchBar 
          {...props}
        />
      </div>
    </Head.Root>
  )
};

export default MainPageHeader;