import styled from 'styled-components';

export const Root = styled.ul`
  margin-bottom: 0;
  display: flex;
  align-items: center;
  list-style-type: none;
  
  @media (max-width: 420px) {
    padding-left: 0;
    margin-top: var(--spacer);
  }
`;

export const Item = styled.li`
  color: var(--primary-black);

  &:not(:last-child) {
    margin-right: var(--spacer);    
  }  

  &:first-child {
    border: 2px solid var(--primary-white);
    color: var(--primary-white);
    background: rgba(0, 0, 0, 0.08);
  }
  
  &:last-child {      
    border: 2px solid var(--primary-black);
    opacity: 0.8;
    background: rgba(0, 0, 0, 0.08);
  }
`;

export const Link = styled.a`
  &:focus {
    outline: var(--focus-border);
  }
  
  &:hover {
    text-decoration: none;
    color: var(--primary-black);
    opacity: 0.8;
    transition: 200ms ease;
  }

  @media (max-width: 420px) {    
    padding: calc(var(--spacer)/2) var(--spacer);
  }
`;

export const LinkStyle: React.CSSProperties = {
  color: 'currentColor',
  fontWeight: 500,
  display: 'inline-block',
  padding: 'calc(var(--spacer)/2) var(--spacer)'
}