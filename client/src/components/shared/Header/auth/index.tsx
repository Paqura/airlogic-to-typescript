import * as React from 'react';
import { Link } from 'react-router-dom';

// Local imports

import * as Auth from './style';

interface IProps {
  isAuthenticated: boolean;
};

export default (props: IProps) => {
  return (
    <Auth.Root>
      { !props.isAuthenticated 
        ? 
          <React.Fragment>
            <Auth.Item>
              <Link 
                to="/login" 
                style={Auth.LinkStyle}
              >
              Login
            </Link>
            </Auth.Item>
            <Auth.Item>
              <Link 
                to="/register" 
                style={Auth.LinkStyle}
              >
                Register
              </Link>
            </Auth.Item>
          </React.Fragment>
        :  
          <Auth.Item>
            <Link 
              to="/cabinet"
              style={Auth.LinkStyle}
            >
              Cabinet
            </Link>
          </Auth.Item>
      }
    </Auth.Root>
  )
};