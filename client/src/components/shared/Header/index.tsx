import * as Loadable from 'react-loadable';

// Local imports

import Loader from 'src/components/shared/Loader';

const MainPageHeader = Loadable({
  loader: () => import('./MainPageHeader'),
  loading: Loader.CircleLoader,
});

const SharedHeader = Loadable({
  loader: () => import('./SharedHeader'),
  loading: Loader.CircleLoader,
});

export default {
  MainPageHeader,
  SharedHeader
}