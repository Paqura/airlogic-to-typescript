import * as React from 'react';
import { Link } from 'react-router-dom';
import * as Logotype from './style';

export default (props: any) => {
  return (
    <Logotype.Root>
      <div>
        <Link 
          to="/" 
          style={Logotype.LinkStyle}
        >
          Airlogica
        </Link>
      </div>
    </Logotype.Root>
  )
}
