import styled from 'styled-components';

export const Root = styled.div``;

export const LinkStyle: React.CSSProperties = {
  fontSize: 'calc(var(--font-size)*1.875)',
  fontWeight: 'bold',
  letterSpacing: '2px',
  color: 'var(--primary-black)'
}

export const Link = styled.a`
  font-size: calc(var(--font-size)*1.875);
  font-weight: bold;
  letter-spacing: 2px;
  color: var(--primary-black);

  &:focus {
    outline: var(--focus-border);
  }

  &:hover,
  &:focus,
  &:active {
    text-decoration: none;
    color: var(--primary-black);
    transition: opacity 200ms ease-in-out;
    opacity: 0.8;
  }
`;