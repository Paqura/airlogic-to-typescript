import * as React from 'react';

// Local imports

import Logo from 'src/components/shared/Header/logo';
import Auth from 'src/components/shared/Header/auth';
import * as HotelHeader from './styles';

interface IProps {
  isAuthenticated: boolean;
};

export default(props: IProps) => {
  const {isAuthenticated} = props;

  return (
    <HotelHeader.SharedRoot>
      <div className="container h-100">
        <HotelHeader.SharedNav>
          <Logo />
          <Auth
            isAuthenticated={isAuthenticated}
          />
        </HotelHeader.SharedNav>
      </div>
    </HotelHeader.SharedRoot>
  )
};
