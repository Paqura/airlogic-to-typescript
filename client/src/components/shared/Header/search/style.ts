import styled from 'styled-components';

export const Root = styled.div `
  padding: calc(var(--spacer)*2);
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  max-width: 460px;
  width: 100%;
  display: flex;  
  flex-wrap: wrap;
  margin: 0 auto;
  box-shadow: var(--card-shadow);
  background-color: var(--primary-white);
  border-radius: 2px;

  @media (max-width: 420px) {
    padding: var(--spacer);
    transform: none;
    top: 70%;
    left: 0;
  }
`;

export const Field = styled.div`
  position: relative;
  display: flex;
  width: 100%;
`;

export const Input = styled.input `
  background-color: rgba(255, 255, 255, 0.2);
  border: none;
  border: 1px solid #e8e8e8;
  padding: 0.375rem 0.75rem;
  border-radius: 2px;
  width: 100%;
  transition: transform 200ms;
  &:focus {
    outline: none;
    background-color: rgba(255, 255, 255, 0.4);
    border-color: rgba(255, 255, 255, 0.4);
    outline: var(--focus-border);
  }
`;

export const Subtitle = styled.span`
  display: inline-block;
  margin-bottom: calc(var(--spacer) * 1.375);
  color: var(--primary-black);
  letter-spacing: 1px;
  font-size: calc(var(--font-size) * 1.375);
  line-height: 1;
  font-weight: 500;
`;
