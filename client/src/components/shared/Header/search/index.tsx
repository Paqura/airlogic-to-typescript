import * as React from 'react';
import { connect } from 'react-redux';

// Local imports

import * as SearchDuck from 'src/ducks/search';

import { SearchButton } from 'src/components/shared/Button';
import * as Search from './style';

interface IProps {
  isLoading: boolean;
  find: (params: { name: string }, history: History) => void;
  history: History;
};

class SearchBar extends React.Component<IProps, any> {
  private innerRef = React.createRef<HTMLInputElement>();

  constructor(props: any) {
    super(props);
    this.innerRef = React.createRef();
  }

  handleClick = (evt: React.SyntheticEvent) => {
    evt.preventDefault();
    
    if(this.innerRef.current) {
      const params = {
        name: this.innerRef.current.value
      }
      this.props.find(params, this.props.history);
    }   
  }

  public render() {
    return (
      <Search.Root>   
        <Search.Subtitle>
          Travel and enjoy with us
        </Search.Subtitle>  
        <Search.Field>
          <Search.Input 
            type="search" 
            id="search"
            name="search"
            placeholder="Enter some place"
            innerRef={this.innerRef}
          />
          <div className="ml-2">
            <SearchButton 
              loadingText={'Wait'}
              isLoading={this.props.isLoading}
              action={this.handleClick}
            />
          </div>
        </Search.Field>
      </Search.Root>
    )
  }
};

const mapStateToProps = (state: any) => ({
  isLoading: state[SearchDuck.moduleName].isLoading
});

export default connect<any, any>(mapStateToProps, null)(SearchBar);

export { 
  SearchBar
}