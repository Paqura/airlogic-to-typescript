import styled from 'styled-components';

export const FormDefault = styled.form`
  
`;

export const FormRoot = styled(FormDefault)`
  box-shadow: var(--card-shadow);
  background-color: var(--primary-white);  
  border-radius: 2px;
  display: flex;
  flex-direction: column;
  position: absolute;
  
  padding: calc(var(--spacer)*2);
  max-width: 480px;
  width: 100%;

  top: 30%;
  left: 50%;
  transform: translate(-50%, -30%);

  @media (max-width: 480px) {
    height: 100%;
    transform: translate(-50%, 0%);
    top: 0;
    justify-content: center;
  }
`;

export const Title = styled.h3`
  font-size: calc(var(--font-size)*2.125);
  margin-bottom: calc(var(--spacer)*1.625);
  font-weight: 500;
`;

export const Background = styled.div`
  width: 100vw;
  height: 100vh;
  background: linear-gradient(135deg, var(--primary-color),#fff);
`;