import styled from 'styled-components';
import { Default }  from './style';

export const Root = styled.div`  
  display: inline-block;
  z-index: 100;
  position: fixed;
  bottom: var(--spacer);
  left: auto;
  right: var(--spacer);
  box-shadow: var(--card-shadow);
  background-color: var(--primary-white);

  @media (max-width: 420px) {
    left: var(--spacer);
    right: var(--spacer);
    position: sticky;
  }  
`;

export const Button: any = styled(Default)`  
  display: flex;
  align-items: center;
  height: 100%;
  padding: calc(var(--spacer)/2) calc(var(--spacer)*2);
  
  &:focus {
    outline: var(--focus-border);
  }

  &::before {
    content: '';
    display: ${(props: {isVisible: boolean}) => !props.isVisible ? 'flex' : 'none'};
    background-image: url('../img/earth.svg');
    background-size: 16px 16px;
    width: 16px;
    height: 16px;
    margin-right: calc(var(--spacer)/2);
  }

  @media (max-width: 420px) {
    padding: var(--spacer) calc(var(--spacer)*2);
    width: 100%;
  }
`;