import * as React from 'react';
import * as Button from './style';

interface IProps {
  action: any;
  isLoading: boolean;
  loadingText: string;
};

export default (props: IProps) => {
  return (
    <Button.SearchButton
      type="button"
      onClick={props.action}
      disabled={props.isLoading}
    >
      { props.isLoading ? props.loadingText : 'Search' }
    </Button.SearchButton>
  )
}
