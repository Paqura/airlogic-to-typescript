import * as React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import {PrimaryButton} from 'src/components/shared/Button/style';

const ButtonWrapper = styled.div`
  position: absolute;
  left: var(--spacer-double);
  top: var(--spacer-double);
  z-index: 1;
`;

const LinkStyle = {
  display: 'flex',
  padding: '4px 16px',
  color: 'var(--primary-black)',
  fontSize: '22px',
  border: '2px dashed var(--primary-black)'
};

function GoBackComponent(props : any) {
  return (
    <ButtonWrapper>
      <Link style={LinkStyle} to="/">Go Back</Link>
    </ButtonWrapper>
  )
}

export default GoBackComponent;