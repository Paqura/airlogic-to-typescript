import * as React from 'react';
import * as Float from './float-style';

interface IProps {
  action: () => void,
  isVisible: boolean
}

const FloatButton = (props: IProps) => {
  return (
    <Float.Root>
      <Float.Button 
        onClick={ props.action } 
        isVisible={props.isVisible}
      >
        { !props.isVisible ? 
          'Currency, terms and another' : 
          'Close' 
        }
      </Float.Button>
    </Float.Root>
  )
}

export default FloatButton;