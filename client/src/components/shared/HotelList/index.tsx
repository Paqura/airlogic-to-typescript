import * as Loadable from 'react-loadable';

// Local imports

import Loader from '../Loader';

const HotelList = Loadable({
  loader: () => import('./HotelList'),
  loading: Loader.CloudLoader,
});

export default HotelList;