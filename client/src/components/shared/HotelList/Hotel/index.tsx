import * as React from 'react';
import { Link } from 'react-router-dom';

// Local imports

import HotelPicture from './HotelPicture';
import HotelTitle from './HotelTitle';
import HotelPrice from './HotelPrice';
import * as Hotel from './style';

export interface IHotel {
  _id: string;
  picture: string;
  title: string;
  price: string;
  facilities?: any;
  isReserve: boolean;
}; 
interface IProps {
  hotel: IHotel
};

export default (props: IProps) => {  
  const {hotel} = props; 
  const url = `/hotel/${hotel._id}`;
  return (
    <Hotel.Root>
      <Link to={url}>
        <HotelPicture 
          pictureSrc={hotel.picture}
        />
        <HotelTitle 
          title={hotel.title}
        />
        <HotelPrice 
          price={hotel.price}
        />
      </Link>      
    </Hotel.Root>    
  )
}

export {
  HotelPicture,
  HotelPrice,
  HotelTitle
}
