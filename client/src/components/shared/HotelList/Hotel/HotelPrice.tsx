import * as React from 'react';
import * as Hotel from './style';

interface IProps {
  price: string;
};

export default (props: IProps) => {
  const { price } = props;
  return (
    <div>
      <Hotel.Price>
        <b>{ price.toString() } ₽ </b> за сутки
      </Hotel.Price>
    </div>
  )
}
