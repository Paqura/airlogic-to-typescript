import styled, { keyframes } from 'styled-components';

export const CssLoader = keyframes`
  from {
    transform: translate(-50%, -50%) rotate(1turn);
  }
  to {
    transform: translate(-50%, -50%) rotate(0turn);
  }
`;

export const Root = styled.li`
  max-width: calc(25% - var(--spacer));
  margin: calc(var(--spacer)/2);
  width: 100%;
  padding: var(--spacer);
  box-shadow: var(--card-shadow);
  transition: box-shadow 200ms ease;

  &:hover > a {
    text-decoration: none;  
  }
  
  @media (min-width: 1140px) {
    &:hover {
      box-shadow: var(--card-hover-shadow);
      transition: box-shadow 200ms ease;
    }
    &:hover  img {
      transform: rotate(2deg) scale(1.2);
    }
  }

  @media (max-width: 420px) {
    max-width: 100%;      
  }
`;

export const BackgroundWrapper = styled.span`
  position: relative;
  width: 100%;
  height: 200px;
  display: flex;
  overflow: hidden;
  
  &::after {
    content: '';
    position: absolute;
    z-index: -1;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    display: flex;
    justify-content: center;
    align-items: center;
    width: 80px;
    height: 80px;
    border-radius: 50%;
    border: 2px solid #ccc;
    border-left-color: transparent;
    animation: ${CssLoader} 800ms infinite;
  }
`;

export const Background = styled.img`
  display: block;
  max-width: 100%;
  height: auto;
  object-fit: cover;
  transition: transform 400ms ease;
  will-change: transform;    
`;

export const Title = styled.span`
  display: inline-block;
  font-weight: 500;
  margin: calc(var(--spacer) / 2) 0;
  color: var(--primary-black);

  &:hover {
    color: var(--primary-color);
  }  
`;

export const Price = styled.span`
  color: var(--primary-black);  
`;