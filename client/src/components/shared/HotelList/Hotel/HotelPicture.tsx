// @flow

import * as React from 'react';
import { Background, BackgroundWrapper } from './style'; 
import LazyLoad from 'react-lazyload';

interface IProps {
  pictureSrc: string;
};

export default (props: IProps) => {
  return (
    <BackgroundWrapper>
      <LazyLoad 
        height={200}
      >
        <Background 
          src={props.pictureSrc} 
          alt="Hotel"
        />
      </LazyLoad>
    </BackgroundWrapper>
  )
}
