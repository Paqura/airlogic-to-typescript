import * as  React from 'react';
import * as Hotel from './style';

interface IProps {
  title: string;
};

export default (props: IProps) => {
  const { title } = props;  
  return <Hotel.Title>{ title }</Hotel.Title>
}
