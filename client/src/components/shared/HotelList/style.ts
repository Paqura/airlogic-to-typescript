import styled from 'styled-components';
import { DefaultList } from 'src/components/shared/List/style';

export const Root = styled.section`
 
`;

export const List = styled(DefaultList)`
  display: flex;
  flex-wrap: wrap;
  margin-left: -16px;
  margin-right: -16px;
`;