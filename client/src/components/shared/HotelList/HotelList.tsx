import * as React from 'react';
import { connect } from 'react-redux';

// SERVICES IMPORTS

import * as HotelDuck from 'src/ducks/hotels';
import * as Hotel from './style';

// LOCAL IMPORTS

import HotelItem, { IHotel } from './Hotel'; 
import Loader from 'src/components/shared/Loader';

// PRECOMPONENT CODE

interface IProps {
  hotels: IHotel[];
  isLoading: boolean;
  lazyLoading: boolean;
  title: string;
};

// COMPONENT CODE

class HotelList extends React.PureComponent<IProps, any> { 
  render() {
    const { hotels, isLoading, lazyLoading, title } = this.props;
    return (   
      <div className="container">
        
        <h4 className="mt-4">{title} </h4>
        <Hotel.Root>
          { isLoading ? 
            <Loader.CloudLoader /> :
            <Hotel.List>
              { hotels && hotels.length ? 
                hotels.map((hotel) => 
                  <HotelItem 
                    key={hotel._id}
                    hotel={hotel}
                  /> ) :
                  <p className="w-100 text-center mt-4">
                    Ничего не найдено...
                  </p>
              }
              { lazyLoading && 
                <div className="w-100 d-flex justify-content-center">
                  <Loader.CloudLoader />
                </div> 
              }
            </Hotel.List>
          }
        </Hotel.Root> 
      </div>
    )
  }  
}

const mapStateToProps = (state: any) => ({
  isLoading: state[HotelDuck.moduleName].isLoading  
});

export default connect(mapStateToProps, null)(HotelList);
