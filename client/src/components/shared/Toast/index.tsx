import * as React from 'react';
import * as Toast from './style';

interface IToast {
  message: string | null;
  action: () => void; 
};

const handleAction = (action: any) => {
  setTimeout(action, 2400);
};

export default (props: IToast) => {
  const { message, action } = props;
  return (
    <Toast.Root>
      { message }
      { handleAction(action) }
    </Toast.Root>
  )
}
