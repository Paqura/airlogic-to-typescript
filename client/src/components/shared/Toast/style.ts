import styled, { keyframes } from 'styled-components';

const BubbleToast = keyframes`
  from {
    transform: translate(-60px, 0);
  }
  to {
    transform: translate(0, 0);
  }
`;

export const Root = styled.div`
  box-shadow: var(--auth-shadow);
  position: fixed;
  border-radius: 2px;
  top: 32px;
  left: 16px;
  width: auto;
  margin-top: 10px;
  max-width: 100%;
  height: auto;
  min-height: 48px;
  line-height: 1.5em;
  background-color: #323232;
  padding: 10px 25px;
  font-size: 1.1rem;
  font-weight: 300;
  color: #fff;
  display: flex;
  align-items: center;
  justify-content: space-between;
  animation: ${BubbleToast} 240ms cubic-bezier(0.165, 0.84, 0.44, 1);
`;