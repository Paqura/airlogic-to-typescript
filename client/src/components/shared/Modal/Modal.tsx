import * as React from 'react';
import styled from 'styled-components';

const ModalWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100vh;
  position: fixed;
  overflow: scroll;
  top: 0;
  left: 0;
  background-color: rgba(0, 0, 0, 0.4);
`;

function Modal (props: any) {
  return (
    <ModalWrapper>
      {props.children}
    </ModalWrapper>
  )
}

export default Modal;