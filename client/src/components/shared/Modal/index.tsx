import * as React from 'react';
import * as ReactDOM from 'react-dom';

import Modal from './Modal';

const portalNode = document.createElement('div');

class ModalProvider extends React.Component {
  componentDidMount() {
    document.body.appendChild(portalNode);
    document.body.classList.add('is-fix');
  }
  
  componentWillUnmount() {
    document.body.removeChild(portalNode);    
    document.body.classList.remove('is-fix');
  }

  render() {
    return ReactDOM.createPortal(
      <Modal {...this.props}/>,
      portalNode
    );
  }
}

export default ModalProvider;