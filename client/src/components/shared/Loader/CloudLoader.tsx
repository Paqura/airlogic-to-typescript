import * as React from 'react';
import styled, { keyframes } from 'styled-components';

const loadingAnimate = keyframes`
  from {
    transform: translateY(-6px);
  }
  to {
    transform: translateY(6px);
  }
`;

export const LoaderWrapper = styled.div`  
  animation: ${loadingAnimate} 4s ease infinite;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 120px;
  margin: 0 auto;
  will-change: transform;
  margin: 16px 0;
`;

export const LoaderPicture = styled.img`
  width: 60px; 
  height: 60px;
`;

export default () => {
  return (
    <LoaderWrapper>
      <LoaderPicture 
        src="./img/cloud.svg"
        alt="Loading..."
      />
    </LoaderWrapper>
  )
}