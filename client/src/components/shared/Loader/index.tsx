import CloudLoader from './CloudLoader';
import CircleLoader from './CircleLoader';

export default {
  CloudLoader,
  CircleLoader
};