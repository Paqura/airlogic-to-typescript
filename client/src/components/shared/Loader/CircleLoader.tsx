import * as React from 'react';
import styled, { keyframes } from 'styled-components';

const circlePit = keyframes`
  from {
    transform: rotate(0);
  }
  to {
    transform: rotate(360deg);
  }
`;

export const LoaderWrapper = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

export const Circle = styled.div`
  width: 80px;
  height: 80px;
  border-radius: 50%;
  border: 2px solid var(--primary-color);
  border-left: 2px solid transparent;
  animation: ${circlePit} 400ms infinite;
`;

export default () => {
  return (
    <LoaderWrapper>
      <Circle />
    </LoaderWrapper>
  )
}
