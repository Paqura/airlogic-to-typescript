import React from 'react';
import Toast from '../../components/shared/Toast';
import { create } from 'react-test-renderer';

test('snapshot test', () => {
  const component = create(<Toast message={'Тестовая ошибка'}/>);
  const tree = component.toJSON()
  expect(tree).toMatchSnapshot(); 
});
