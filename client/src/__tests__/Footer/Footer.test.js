// @ts-ignore

import React from 'react';
import { create } from 'react-test-renderer';
import { FloatButton } from '../../components/shared/Button'; 

it('Snapshot of footer button with standart value', () => {
  const component = create(
    <FloatButton 
      action={() => 'hello'}
      isVisible={false}
    />
  );
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot(); 
});

it('Snapshot of footer button with close value', () => {
  const component = create(
    <FloatButton 
      action={() => 'hello'}
      isVisible={true}
    />
  );
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot(); 
});
