// @ts-ignore

import React from 'react';
import { create } from 'react-test-renderer';
import Modal from '../../components/shared/Modal/Modal'; 

it('Snapshot of modal with fake inner', () => {
  const fakeModalInner = <div>Modal bubble</div>;
  const component = create(
    <Modal>
      {fakeModalInner}
    </Modal>
  );
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot(); 
});