// @ts-ignore

import React from 'react';
import { create } from 'react-test-renderer';
import Enzyme, { shallow, render, mount } from 'enzyme'; 
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });


import {SearchButton} from '../../components/shared/Button';


describe("SearchButton snapshot test", () => {
  let wrapper;
  const output = 10;
  const props = {
    loadingText: 'wait',
    isLoading: false,
    action: () => 'fakeActionValue'
  };

  beforeEach(()=>{
    wrapper = shallow(<SearchButton {...props}/>)
  })

  it("should be return snapshot with 'search' text", () => {  
    const component = create(
      <SearchButton {...props}/>
    );
  
    const tree = component.toJSON();  
    expect(tree).toMatchSnapshot();
  });

  it("should be return snapshot with 'wait' text", () => {  
    props.isLoading = true;
    const component = create(
      <SearchButton {...props}/>
    );
  
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});

