// @ts-ignore

import React from 'react';
import { createPortal } from 'react-dom';
import { create } from 'react-test-renderer';

import { 
  HotelPrice, 
  HotelTitle 
} from '../../components/shared/HotelList/Hotel';

describe('Render Hotel Components', () => {
  it('Render Hotel Price', () => {
    const component = create(<HotelPrice price={'125'} />);
    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('Render Hotel Title', () => {
    const component = create(<HotelTitle title={'FakeTitle'} />);
    const tree = component.toJSON();

    expect(tree).toMatchSnapshot();
  });
});