import * as React from 'react'
import { Field, reduxForm } from 'redux-form';
import TextField from 'src/components/shared/TextField';
import * as AuthForm from 'src/components/shared/Form/style';
import { FormButton } from 'src/components/shared/Button/style';
import {GoBackButton} from 'src/components/shared/Button';
import validate from '../../utils/validate';

const RegisterForm = (props: any) => {
  const { handleSubmit, pristine, submitting, invalid, isRequest } = props;
  return (
    <AuthForm.Background>
      <GoBackButton />
      <AuthForm.FormRoot onSubmit={handleSubmit}>    
        <header>
          <AuthForm.Title>Register</AuthForm.Title>  
        </header>         
        <Field
          name="name"
          component={TextField}
          type="text"            
          id="name-register" 
          label="name"             
        />                
        <Field
          name="email"
          component={TextField}
          type="email"            
          id="register-email"
          label="email"     
        />        
        <Field
          name="password"
          component={TextField}
          type="password"
          id="register-password"
          label="password"     
        />
        <div>
          <FormButton 
            type="submit" 
            disabled={isRequest || pristine || submitting || invalid}
          >
            { isRequest ? 'Submiting' : 'Submit' }
          </FormButton>
        </div>
      </AuthForm.FormRoot>
    </AuthForm.Background>
  )
}

export default reduxForm({
  form: 'register',
  validate
})(RegisterForm)