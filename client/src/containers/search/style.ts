import styled from 'styled-components';
import * as Search from 'src/components/shared/Header/search/style';

export const Root = styled(Search.Root)`
  transform: none;
  position: static;
`;
