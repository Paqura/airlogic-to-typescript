import * as React from 'react';

// Local imports

import * as Search from 'src/components/shared/Header/search/style';
import * as SearchPage from './style';

import { SearchButton } from 'src/components/shared/Button';

class SearchBar extends React.Component<any, any> {
  private innerRef = React.createRef<HTMLInputElement>();

  constructor(props: any) {
    super(props);
    this.innerRef = React.createRef();
  }

  handleClick = (evt: any) => {
    evt.preventDefault();
    if(this.innerRef.current) {
      const params = {
        name: this.innerRef.current.value
      };
      this.props.findHotelsRequest(params, this.props.history);
    }
  }

  render() {
    return (
      <SearchPage.Root>
        <Search.Subtitle>
          Traveling and enjoing with us
        </Search.Subtitle>
        <Search.Field>
          <Search.Input 
            type="search" 
            id="search"
            name="search"
            placeholder="Enter some place"
            innerRef={this.innerRef}
          />
          <div className="ml-2">
            <SearchButton 
              action={this.handleClick}
              isLoading={this.props.isLoading}
              loadingText={'Searching...'}
            />
          </div>
        </Search.Field>
      </SearchPage.Root>
    )
  }
}

export default SearchBar;
