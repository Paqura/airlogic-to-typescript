import * as React from 'react';

// Local imports

import ModalProvider from 'src/components/shared/Modal';
import Header from 'src/components/shared/Header';
import Album from './album';
import Info from './info';
import HotelPlacemark from './map';
import HotelReserve from './reserve';
import HotelComment from './comments';
import Dates from './dates';
import LoadableSlider from './slider/LoadableSlider';

// INTERFACES

import { IHotel } from 'src/components/shared/HotelList/Hotel';
interface IProps {
  isAuthenticated: boolean;
  hotel: IHotel;
  reserveHotelWithoutAuth: () => void;
};

interface IState {
  sliderVisible: boolean;
  datesVisible: boolean;
};
class Hotel extends React.Component<IProps, IState> {
  public sliderVisible: boolean;
  
  state = {
    sliderVisible: false,
    datesVisible: false
  }
  
  openSlider = (evt: any) => {
    this.setState({
      sliderVisible: true
    })
  }

  closeSlider = (evt: any) => {
    this.setState({
      sliderVisible: false
    })
  }

  reserveHotel = () => {
    if(!this.props.isAuthenticated) {
      return this.props.reserveHotelWithoutAuth();
    }
    this.setState({
      datesVisible: true
    })
  }

  closeDates = () => {
    this.setState({
      datesVisible: false
    })
  }

  render() {
    const { isAuthenticated, hotel } = this.props;
    return (
      <div>
        { this.state.sliderVisible && 
          <ModalProvider>
            <LoadableSlider closeSlider={this.closeSlider} />
          </ModalProvider> }
        <Header.SharedHeader isAuthenticated={isAuthenticated} />
        <div className="container">
          <Album 
            picture={hotel.picture} 
            title={hotel.title}
            openSlider={this.openSlider}
          />
          <div className="row m-32">
            <div className="col-12 col-md-6">
              <Info facilities={hotel.facilities} />
              <HotelComment />
            </div>
            <div className="col-12 col-md-6 sticky-container">
              <HotelPlacemark hotel={hotel} />
              <HotelReserve 
                price={hotel.price}
                reserveHotel={this.reserveHotel}
                isReserve={hotel.isReserve} 
              />
            </div>
          </div>
        </div>
        {this.state.datesVisible && 
          <ModalProvider>
            <Dates 
              hotelId={hotel._id} 
              closeDates={this.closeDates}
            />
          </ModalProvider>
          }
      </div>
    )
  }
}

export default Hotel;

export {
  Header,
  Album
}
