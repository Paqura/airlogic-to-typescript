import styled from 'styled-components';

export const Root = styled.div`
  padding: var(--spacer);
  box-shadow: var(--card-shadow);
  margin: var(--spacer) 0;

  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const ReservePrice = styled.span`
  display: inline-block;
  font-size: calc(var(--font-size) * 1.375);
  font-weight: 500;
  margin-right: auto;
`;

export const PostPrice = styled.small`
  font-size: calc(var(--font-size) * 0.875);
  margin-left: calc(var(--spacer)/2);
`;