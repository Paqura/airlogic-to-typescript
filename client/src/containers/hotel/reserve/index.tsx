import * as React from 'react';

// Local imports

import * as Reserve from './style';
import * as Button from 'src/components/shared/Button/style';

interface IProps {
  price: string;
  reserveHotel: (id: string) => void;
  isReserve: boolean;
};

export default (props: IProps) => {
  return (
    <Reserve.Root>
      <Reserve.ReservePrice>
        {props.price}₽
        <Reserve.PostPrice>
          in a day
        </Reserve.PostPrice>
      </Reserve.ReservePrice>
      <Button.SecondaryButton
        onClick={props.reserveHotel} 
        disabled={props.isReserve}
      >
        {props.isReserve ? 'Place is reserved' : 'Reserve this place'}
      </Button.SecondaryButton>
    </Reserve.Root>
  )
}
