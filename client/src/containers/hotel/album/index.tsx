import * as React from 'react';
import * as Album from './style';
import * as Button from 'src/components/shared/Button/style';

interface IProps {
  openSlider: (evt: any) => void;
  title: string;
  picture: string;
};

export default (props: IProps) => {
  const randomText = `Lorem ipsum dolor sit amet consectetur 
    adipisicing elit. Vero, facilis necessitatibus 
    rem sapiente dolor in excepturi nisi 
    amet dolores. Rerum!
  `;
  return (
    <Album.Root>
      <Album.InfoWrapper>
        <Album.InfoTitle>
          {props.title}
        </Album.InfoTitle>
        <Album.Description>
          {randomText}
        </Album.Description>
        <Album.Description>
          {randomText}
        </Album.Description>
        <p className="mt-4">
          <Button.PrimaryButton
            onClick={props.openSlider}
          >
            Check more photos
          </Button.PrimaryButton>
        </p>
      </Album.InfoWrapper>
      <Album.ImageWrapper>
        <Album.FullScreenPicture
          src={props.picture}
          alt="Фотография отеля"
        />
      </Album.ImageWrapper>
    </Album.Root>
  )
}
