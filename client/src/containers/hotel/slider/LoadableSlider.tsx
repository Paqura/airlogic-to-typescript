import * as Loadable from 'react-loadable';

// Local

import Loader from "src/components/shared/Loader";

const LoadableSlider = Loadable({
  loader: () => import('./index'),
  loading: Loader.CircleLoader,
});

export default LoadableSlider;