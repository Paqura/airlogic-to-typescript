import styled from 'styled-components';
import { Default } from 'src/components/shared/Button/style';

export const SliderWrapper = styled.section`
  position: fixed;
  display: flex;
  justify-content: center;
  align-items: center;
  top: 0;
  left: 0;
  background-color: rgba(0, 0,0, 0.3);
  height: 100%;
  width: 100%;
  z-index: 101;
  overflow: scroll;
`;

export const SliderInner = styled.div`
  max-width: 800px;
  width: 100%;
`;

export const SlidePicture = styled.img`
  max-height: 500px;
  width: 100%;
  object-fit: cover !important;

  @media (max-width: 420px) {
    height: 100vh !important;
    max-height: 100vh;
  }
`;

export const CloseButton = styled(Default)`
  position: absolute;
  right: var(--spacer);
  top: var(--spacer);
  transition: transform 200ms;
  z-index: 101;

  &:hover {
    transform: rotate(0.5turn);
  }

  & img {
    width: 64px;
    height: 64px;
  }
`;