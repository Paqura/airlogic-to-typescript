import * as React from "react";
import LazyLoad from 'react-lazyload';
import Slider from "react-slick";

// Local imports

import * as CustomSlider from './style';

import slides from './mocks';

class SimpleSlider extends React.Component<any, any> {
  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };

    return (
      <CustomSlider.SliderInner>
        <CustomSlider.CloseButton 
          onClick={this.props.closeSlider}
        >
          <img 
            src="./img/close.svg" 
            alt="Закрыть окно" 
          />
        </CustomSlider.CloseButton>
        <Slider {...settings}>
          {slides.map(it => 
            <div key={it}>
              <LazyLoad
                height={500}
              >
                <CustomSlider.SlidePicture 
                  src={it} 
                  alt="Фото"
                />
              </LazyLoad>              
            </div>  )}
        </Slider>
      </CustomSlider.SliderInner>
    );
  }
}

export default SimpleSlider;

