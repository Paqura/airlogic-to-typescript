import * as React from 'react';
import { DateRangePicker } from 'react-dates';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import * as moment from 'moment';
import { connect } from 'react-redux';

// Local imports

import * as Button from 'src/components/shared/Button/style';
import * as DatesComponent from './style';

import * as HotelDuck from 'src/ducks/hotel';
import * as AuthDuck from 'src/ducks/auth';

interface IReservePayload {
  startDate: any;
  endDate: any;
  id: string;
};
interface IState {
  startDate: any;
  endDate: any;
  focusedInput: any; 
  errorText: boolean;
}

interface IProps {
  closeDates: () => void;
  reserveHotel: (payload: IReservePayload) => void;
  hotelId: string;
  initialStartDate: null;
  initialEndDate: null; 
}

class Dates extends React.PureComponent<any, IState> {
  static defaultProps = {
    initialStartDate: null,
    initialEndDate: null
  }

  state = {
    startDate: this.props.initialStartDate,
    endDate: this.props.initialEndDate, 
    focusedInput: null,
    errorText: false
  }

  dateChange = ({ startDate, endDate }: any) => {
    this.setState({ startDate, endDate });
  }

  reserve = () => {
    const {startDate, endDate} = this.state;

    if(!startDate || !endDate) {
      this.setState({
        errorText: true
      })
      return;
    } else {
      this.setState({
        errorText: false
      });

      const payload = {
        startDate,
        endDate,
        hotelId: this.props.hotelId,
        userId: this.props.user.userId
      };
  
      this.props.reserveHotel(payload);
      this.props.closeDates();
      this.props.reserveHotelSucess();
    }
  }

  render() {
    return (
      <DatesComponent.Inner>
        <DatesComponent.Title>Choice dates</DatesComponent.Title>
        <DatesComponent.InputWrapper>
          <DatesComponent.SubTitle>Select time for reserve</DatesComponent.SubTitle>
          <DateRangePicker
            startDate={this.state.startDate}
            startDateId="START_DATE"
            endDate={this.state.endDate}
            endDateId="END_DATE"
            onDatesChange={this.dateChange} 
            focusedInput={this.state.focusedInput}
            onFocusChange={focusedInput => this.setState({ focusedInput })} 
            numberOfMonths={1} 
          />
        </DatesComponent.InputWrapper>
        <div className="d-flex justify-content-end">
          <Button.CanselButton onClick={this.props.closeDates}>
            Close
          </Button.CanselButton>
          <span className="ml-2">
            <Button.SecondaryButton onClick={this.reserve}>
              Accept
            </Button.SecondaryButton>
          </span>
        </div>
        {this.state.errorText && 
          <DatesComponent.Error>Date is required</DatesComponent.Error>}
      </DatesComponent.Inner>
    )
  }
}

const mapStateToProps = (state: any) => ({
  user: state[AuthDuck.moduleName].user
})

const { reserveHotel, reserveHotelSucess } = HotelDuck;

const mapDispatchToProps = ({
  reserveHotel,
  reserveHotelSucess
});

export default connect(mapStateToProps, mapDispatchToProps)(Dates);
