import styled from 'styled-components';

export const Inner = styled.div`
  padding: 32px;
  max-width: 600px;
  margin: 0 auto;
  background-color: var(--primary-white);
  box-shadow: var(--card-shadow);
  border-radius: 4px;

  @media (max-width: 420px) {
    max-width: 96%;
  }
`;

export const Title = styled.h3`
  display: block;
  width: 100%;
  margin-bottom: 24px;
`;

export const SubTitle = styled.h6`  
  display: block;
  width: 100%;
  text-transform: uppercase;
`;

export const InputWrapper = styled.div`
  margin-bottom: 16px;
`;

export const Error = styled.span`
  color: var(--primary-red);
  display: block;
  width: 100%;
  margin-top: 16px;
  padding: 8px 16px;
  background-color: #fff2ed;
`;