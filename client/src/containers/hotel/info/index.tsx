import * as React from 'react';
import FacilitiesList from './facilities';
import * as Facilities from './style';

interface IFacilities {
  view: string;
};
interface IProps {
  facilities: IFacilities
};

export default (props: IProps) => {
  const facilities = props.facilities[0];  
  return (
    <Facilities.Root>
      <Facilities.Head>
        <h4>Facilities</h4>
      </Facilities.Head>
      <FacilitiesList 
        facilities={facilities}
      />
    </Facilities.Root>
  )
}
