import * as React from 'react';
import * as Facilities from './style';

interface IView {
  counter: boolean;
  name: string;
  props?: string;
};

const views: IView[] = [
  { 
    counter: true,
    name: 'beds',
    props: 'bedsCount'
  },
  {
    counter: false,
    name: 'wifi'
  },
  {
    counter: false,
    name: 'parking'
  },
  {
    counter: false,
    name: 'tv'
  },
  {
    counter: false,
    name: 'kitchen'
  }
];

const renderFacilities = (view: any, facilities: any) => {
  const url = `./img/${view.name}.svg`;
  return(
    <Facilities.Item key={view.name}>
      <div>
        <Facilities.Icon 
          src={url}
          alt={view.name}
        />
      </div>
      <Facilities.IconDescription>
        { view.name }:  
        { view.counter ? 
          `${facilities[view.props]}` : 
            facilities[view.name] ? 
              ' yes' : 
              ' no'
        }
      </Facilities.IconDescription>  
    </Facilities.Item>  
  )
}

export default ({ facilities }: any) => {
  return (
    <Facilities.List>
      { views.map(view => renderFacilities(view, facilities)) }
    </Facilities.List>
  )
}
