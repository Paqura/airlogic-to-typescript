import styled from 'styled-components';

export const Root = styled.div`
  padding: calc(var(--spacer) * 2);
  box-shadow: var(--card-shadow);
  max-width: 100%;
  width: auto;

  @media (max-width: 420px) {
    padding: var(--spacer);
  }
`;

export const Head = styled.header`
  margin-bottom: calc(var(--spacer)*1.375);
  color: var(--primary-black);

  @media (max-width: 420px) {
    margin: var(--spacer) 0;
    font-size: calc(var(--font-size) * 1.125);
  }
`;

export const List = styled.ul`
  display: flex;
  list-style-type: none;
  padding: 0;
  margin: 0 -16px;

  @media (max-width: 420px) {
    flex-direction: column;
    align-items: flex-start;
  }
`;

export const Item = styled.li`
  text-align: center;
  padding: 0 var(--spacer);

  @media (max-width: 420px) {
    display: flex;
    text-align: left;
    padding: var(--spacer);
  }
`;

export const Icon = styled.img`
  width: 36px;
  height: 36px;
`;

export const IconDescription = styled.span`
  display: block;
  margin-top: calc(var(--spacer)/2);
  font-weight: 500;
  
  @media (max-width: 420px) {
    margin-left: var(--spacer);
  }
`;