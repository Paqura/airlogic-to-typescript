import * as React from 'react'
import { Field, reduxForm } from 'redux-form';

// Local imports

import validate from 'src/utils/validate';

import * as AuthForm from 'src/components/shared/Form/style';
import {GoBackButton} from 'src/components/shared/Button';
import TextField from 'src/components/shared/TextField';
import { FormButton } from 'src/components/shared/Button/style';

const LoginForm = (props: any) => {
  const { handleSubmit, pristine, submitting, invalid, isRequest } = props;
  return (
    <AuthForm.Background>     
      <GoBackButton />
      <AuthForm.FormRoot onSubmit={handleSubmit}>        
        <header>
          <AuthForm.Title>
            Log in
          </AuthForm.Title>
        </header>             
        <Field
          name="email"
          component={TextField}
          type="email"            
          id="register-email"
          label="email"     
        />        
        <Field
          name="password"
          component={TextField}
          type="password"
          id="register-password"
          label="password"     
        />
        <div>
          <FormButton 
            type="submit" 
            disabled={pristine || submitting || invalid || isRequest}
          >
            { isRequest ? 'Submiting' : 'Submit' }
          </FormButton>       
        </div>
      </AuthForm.FormRoot>
    </AuthForm.Background>
  )
};

export default reduxForm({
  form: 'login',
  validate
})(LoginForm)

export {
  LoginForm
}