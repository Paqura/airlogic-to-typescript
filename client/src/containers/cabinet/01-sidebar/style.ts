import styled from 'styled-components';

export const Root = styled.aside`  
  position: sticky;
  top: 0; 
  padding: var(--spacer-double) 0;
  height: 100vh;
  box-shadow: var(--card-shadow);
  background-color: #444852;
`;

export const Inner = styled.div`
  
`;

