import styled from 'styled-components';

export const Root = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: var(--spacer-double);
`;

export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  max-height: 40px;
  height: 100%;
  max-width: 40px;
  width: 100%;
  border-radius: 50%;
  overflow: hidden;
`;

export const Picture = styled.img`
  max-width: 100%;
  height: auto;
  object-fit: cover;
`;

export const UserName = styled.span`
  display: block;
  margin-top: var(--spacer);
  text-align: center;
  font-weight: 500;
  letter-spacing: 0.6px;
  color: var(--primary-white);
`;