import * as React from 'react';

// STYLE IMPORTS

import * as Avatar from './style';

// PRECOMPONENT CODE
interface IAvatarProps {
  avatarUrl?: string;
  userName: string;
  email: string;
};

// COMPONENT CODE

function AvatarComponent(props: IAvatarProps) {
  const {avatarUrl, userName, email} = props;
  const fakeAvatar = './img/fake-avatar.png';
  return(
    <Avatar.Root>
      <Avatar.Wrapper>
        <Avatar.Picture 
          src={avatarUrl ? avatarUrl : fakeAvatar} 
          alt="Аватар пользователя"
        />
      </Avatar.Wrapper>
      <Avatar.UserName>
        {userName}
        <br/>
        {email}
      </Avatar.UserName>
    </Avatar.Root>
  )
};

export default AvatarComponent;