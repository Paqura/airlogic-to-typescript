import * as React from 'react';
import { NavLink } from 'react-router-dom';

// STYLES IMPORTS

import * as Nav from './style';

// PRECOMPONENT CODE

export interface ILink {
  id: string;
  url: string;
  title: string;
  icon_path: string;
};

function NavItem(link: ILink) { 
  return(
    <Nav.Item key={link.id}>
      <NavLink 
        exact={true}
        className="cabinet-navlink" 
        to={link.url} 
        activeStyle={{
          borderColor: 'var(--primary-color)',
          backgroundColor: '#3A3D45'
        }}
      >  
        <Nav.IconWrapper>
          <Nav.Icon 
            src={link.icon_path} 
            alt="Иконка" 
            width="24" height="24"
          />
        </Nav.IconWrapper>
        {link.title}
      </NavLink>
    </Nav.Item>
  )
};

export default NavItem;
