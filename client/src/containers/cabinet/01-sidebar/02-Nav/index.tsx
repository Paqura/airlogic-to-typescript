import * as React from 'react';

// LOCAL IMPORTS

import NavItem from './NavItem'; 

// STYLES IMPORTS

import * as Nav from './style';

// PRECOMPONENT CODE

import { ILink } from './NavItem';
import links from './links';

// COMPONENT CODE

function NavComponent() {  
  return(
    <Nav.Root>
      <Nav.List>
        {links.map((link: ILink) => 
          <NavItem 
            key={link.id}
            id={link.id}
            url={link.url} 
            title={link.title} 
            icon_path={link.icon_path} 
          />
        )}
      </Nav.List>
    </Nav.Root>
  )
};

export default NavComponent;