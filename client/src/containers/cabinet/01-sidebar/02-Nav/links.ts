export default [
  {
    id: '1',
    url: '/cabinet',
    title: 'Orders',
    icon_path: './img/list.svg'
  },
  {
    id: '2',
    url: '/cabinet/settings',
    title: 'Settings',
    icon_path: './img/settings.svg'
  }, 
  {
    id: '3',
    url: '/',
    title: 'exit',
    icon_path: './img/exit.svg'
  }
];