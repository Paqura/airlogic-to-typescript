import styled from 'styled-components';

export const Root = styled.nav`
  padding: var(--spacer-double) 0;
`;

export const List = styled.ul`
  list-style-type: none;
  padding: 0;  
`;

export const Item = styled.li`
`;

export const IconWrapper = styled.span`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Icon = styled.img`
  margin-right: var(--spacer);
`;
