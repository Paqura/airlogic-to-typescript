import * as React from 'react';

// LOCAL IMPORTS

import AvatarComponent from './01-Avatar';
import NavComponent from './02-Nav';

// STYLES IMPORTS

import * as Sidebar from './style';

// COMPONENT CODE

function SidebarComponent(props: any) {
  return(
    <Sidebar.Root>
      <Sidebar.Inner>
        <AvatarComponent 
          avatarUrl={props.userInfo.avatar}
          userName={props.userInfo.name}
          email={props.userInfo.email}
        />
        <NavComponent />
      </Sidebar.Inner>
    </Sidebar.Root>
  )
}

export default SidebarComponent;