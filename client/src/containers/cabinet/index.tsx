import * as React from 'react';
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';

// SERVICES IMPORTS

import * as AuthDuck from 'src/ducks/auth';
import * as CabinetDuck from 'src/ducks/cabinet';

// LOCAL IMPORTS

import SidebarCopmonent from 'src/containers/cabinet/01-sidebar';
import OrdersComponent from 'src/containers/cabinet/02-orders';
import DetailComponent from 'src/containers/cabinet/03-detail';

// STYLES IMPORTS

import * as Cabinet from './style';

// COMPONENT CODE

class CabinetComponent extends React.PureComponent<any, any> {
  render() {
    const {userInfo} = this.props;
    return(
      <Cabinet.Root>
        <SidebarCopmonent userInfo={userInfo} />
        {userInfo.userId && 
          <Cabinet.ContentSection>
            <Route 
              exact={true} 
              path="/cabinet" 
              render={(props) => 
                <OrdersComponent 
                  fetchOrders={this.props.fetchOrders} 
                  userId={userInfo.userId}
                  {...props}/>
                }
            />
            <Route 
              exact={true} 
              path="/cabinet/:id" 
              render={(props) => 
                <DetailComponent
                  {...props}/>
              }
            />
          </Cabinet.ContentSection>
        }
      </Cabinet.Root>
    )
  }
};

const mapStateToProps = (state: any) => ({
  userInfo: state[AuthDuck.moduleName].user
});

const {fetchOrders} = CabinetDuck;

const mapDispatchToProps = ({
  fetchOrders
});

export default connect(mapStateToProps, mapDispatchToProps)(CabinetComponent); 