import * as React from 'react';
import styled from 'styled-components';

// PRECOMPONENT CODE

const Title = styled.h2`
  margin-bottom: var(--spacer-third);
  color: var(--primary-black);
  border-left: 4px solid var(--primary-color);
  padding-left: var(--spacer);
  font-weight: 400;
  letter-spacing: 1px;
`;

// COMPONENT CODE

function TitleComponent() {
  return(
    <header>
      <Title>Your orders</Title>
    </header>
  )
}

export default TitleComponent;