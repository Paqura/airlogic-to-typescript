import styled from 'styled-components';

export const Root = styled.div`
  display: grid;
  grid-template-columns: 1fr 5fr;
`;

export const ContentSection = styled.div`
  padding: var(--spacer-double);
  background-color: #fafafa;
  display: flex;
  justify-content: center;
`; 