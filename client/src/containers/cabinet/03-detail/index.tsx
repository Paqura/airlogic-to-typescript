import * as React from 'react';
import { connect } from 'react-redux';

// SERVICES IMPORTS

import * as CabinetDuck from 'src/ducks/cabinet';

// COMPONENT CODE

class DetailComponent extends React.Component<any, any> {
  componentDidMount() {
    const {id} = this.props.match.params;
    this.props.fetchDetail(id);
  }

  render() {
    const {detail} = this.props;
    return detail && 
          <span>{detail.title}</span>
  }
};

const mapStateToProps = (state: any) => ({
  detail: state[CabinetDuck.moduleName].detail
});

const {fetchDetail} = CabinetDuck;

const mapDispatchToProps = ({
  fetchDetail
});

export default connect(mapStateToProps, mapDispatchToProps)(DetailComponent);
