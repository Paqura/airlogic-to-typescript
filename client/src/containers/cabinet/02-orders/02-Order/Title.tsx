import * as React from 'react';
import styled from 'styled-components';

// STYLES IMPORTS

import * as Order from './style';

// PRECOMPONENT CODE

export const Title = styled.h2`
  padding: var(--spacer);
  text-align: center;
  margin-top: var(--spacer-half);
  font-size: 20px;
  font-weight: 400;
  letter-spacing: 1px;
  min-height: 80px;
`;

interface ITitle {
  title: string;
};

// COMPONENT CODE

function TitleComponent(props: ITitle) {
  return(
    <header>
      <Title>
        {props.title}
      </Title>
    </header>
  )
}

export default TitleComponent;