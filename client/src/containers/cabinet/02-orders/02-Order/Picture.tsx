import * as React from 'react';
import styled from 'styled-components';

// PRECOMPONENT CODE 

export const PictureWrapper = styled.span`
  display: flex;
  height: 200px;
`;

export const Picture = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  box-shadow: var(--card-shadow);
`;

interface IPicture {
  picture: string;
  title: string;
};

// COMPONENT CODE

function PictureComponent(props: IPicture) {
  return(
    <PictureWrapper>
      <Picture 
        src={props.picture} 
        alt={props.title}
      />
    </PictureWrapper>
  ) 
}

export default PictureComponent;