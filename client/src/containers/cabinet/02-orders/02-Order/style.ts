import styled from 'styled-components';

export const Root = styled.li`
  max-width: calc(100%/4 - 32px);
  margin: var(--spacer);

  &:not(:last-child) {
    margin-bottom: var(--spacer-double);
  }

  @media (max-width: 1440px) {
    max-width: calc(100%/3 - 32px);
  }

  @media (max-width: 1128px) {
    max-width: calc(100%/2 - 32px);
  }
`;

export const Card = styled.div`
  display: flex;
  flex-direction: column;  
  align-items: center;
  background-color: var(--primary-white);
  border-radius: 4px;
  box-shadow: var(--card-shadow);

  padding-bottom: var(--spacer-double);

  & a {
    font-size: 18px;
    font-weight: 500;
    letter-spacing: 1px;
    text-transform: uppercase;
    color: var(--primary-blue);
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: var(--primary-white);
    border: 2px dashed var(--primary-blue);
    padding: var(--spacer-half) var(--spacer-double);

    &:hover {
      text-decoration: none;
      border: 2px solid var(--primary-blue);
    }
  }
`;

export const Redirect = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: var(--spacer-double);
  background-color: var(--primary-blue);
  border-radius: 4px;

  margin-top: var(--spacer-half);
  box-shadow: var(--card-shadow);

  & a {
    color: var(--primary-white);
    font-size: 22px;
  }
`;

