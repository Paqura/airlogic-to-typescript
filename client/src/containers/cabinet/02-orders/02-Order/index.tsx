import * as React from 'react';
import {Link} from 'react-router-dom';

// LOCAL IMPORTS

import PictureComponent from './Picture';
import TitleComponent from './Title';

// STYLES IMPORTS

import * as Order from './style';

// PRECOMPONENT CODE

export interface IOrder {
  _id : string;
  title : string;
  picture : string;
};

// COMPONENT CODE

function OrderComponent(props : IOrder) {
  const {_id, title, picture} = props;
  const hotelpageId = `/hotel/${_id}`;
  const cabinetpageId = `/cabinet/${_id}`;

  return (
    <Order.Root key={_id}>
      <Order.Card>
        <PictureComponent 
          picture={picture}
          title={title}
        />
        <TitleComponent title={title} />
        <Link to={cabinetpageId}>
          Detail
        </Link>
      </Order.Card>

      <Order.Redirect>
        <Link to={hotelpageId}>
          Jump to hotel
        </Link>
      </Order.Redirect>      
    </Order.Root>
  )
}

export default OrderComponent;
