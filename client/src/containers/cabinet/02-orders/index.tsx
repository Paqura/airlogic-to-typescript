import * as React from 'react';
import { connect } from 'react-redux';

// SERVISES IMPORTS

import * as CabinetDuck from 'src/ducks/cabinet';

// LOCAL IMPORTS

import TitleComponent from 'src/containers/cabinet/00-shared/01-Title';
import OrderListComponent from 'src/containers/cabinet/02-orders/01-OrderList';
import Loader from 'src/components/shared/Loader';

// COMPONENT CODE

class OrdersComponent extends React.Component<any, any> {  
  componentDidMount() {
    this.props.fetchOrders(this.props.userId);
  }

  render() {
    const {orders} = this.props;
    return(
      <div>
        <TitleComponent />
        { orders.length ? 
          <OrderListComponent orders={orders}/> :
          <Loader.CircleLoader />
        }
      </div>
    )
  } 
};

const mapStateToProps = (state: any) => ({
  orders: state[CabinetDuck.moduleName].orders
});

export default connect(mapStateToProps, null)(OrdersComponent);