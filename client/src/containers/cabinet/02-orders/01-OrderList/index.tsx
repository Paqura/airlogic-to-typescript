import * as React from 'react';
import styled from 'styled-components';

// Local imports 

import OrderComponent, { IOrder } from 'src/containers/cabinet/02-orders/02-Order';

// PRECOMPONENT CODE

interface IProps {
  orders: IOrder[];
}

const List = styled.ul`
  display: flex;
  flex-wrap: wrap;
  margin-left: -16px;
  margin-right: -16px;
  
  list-style-type: none;
  padding: 0;
`;

// COMPONENT CODE

function OrderListComponent(props: IProps) {
  return(
    <List>
      {props.orders.map((order: IOrder) => {
        const {picture, title, _id} = order;
        return (
          <OrderComponent 
            key={_id}
            picture={picture}
            title={title}
            _id={_id}
          />
        )
      })} 
    </List>
  )
};

export default OrderListComponent;