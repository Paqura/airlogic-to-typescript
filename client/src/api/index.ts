export const AUTH = {
  LOGIN: '/api/auth/login',
  REGISTER: '/api/auth/register'
}; 

export const HOTEL = {
  CURRENT: '/api/hotel/'
};

export const HOTELS = {
  FIRST_REQUEST: `/api/hotel/?offset=0&limit=8`,
  RESERVE: `api/hotel/reserve/`
};

export const RESERVE = {
  DEFAULT: '/api/reserve/',
  FIND_IDS: 'api/reserve/list/'
};