import * as jwt_decode from 'jwt-decode';

import setAuthToken from 'src/utils/setAuthToken';
import { setCurrentUser, logoutUser } from 'src/ducks/auth';

interface IDecoded {
  email: string;
  userId: string;
  iat: number;
  exp: number;
};

const localStorageMiddleware =  (store: any) => (next: any) => async (action: any) => {
  if(action && action.type && action.type === 'CHECK_USER_TOKEN') {
    if (localStorage.jwtToken) {
      setAuthToken(localStorage.jwtToken); 
      const decoded: IDecoded = jwt_decode(localStorage.jwtToken);
      store.dispatch(setCurrentUser(decoded));
      const currentTime = Date.now() / 1000;

      if(decoded.exp < currentTime) {
        store.dispatch(logoutUser());
      }

    
    }
  }
  if(action && action.type) {
    next(await action);  
  } else {
    next({
      type: 'EMPTY_ACTION'
    });  
  }  
};


export { localStorageMiddleware };