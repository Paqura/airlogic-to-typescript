import * as React from 'react';
import { connect } from 'react-redux';
import * as Loadable from 'react-loadable';

// Local imports

import { loginFormRequest, moduleName } from 'src/ducks/auth';
import Loader from 'src/components/shared/Loader';

const LoadableLoginForm = Loadable({
  loader: () => import('src/containers/login'),
  loading: Loader.CloudLoader,
});

export interface IProps {
  submitState?: (submitState: boolean) => boolean;
  isAuthenticated: boolean;
  loginFormRequest: ({}, history: any) => void;
  history: any;
};

class Login extends React.Component<any, any> {  
  componentDidMount() {
    if(this.props.isAuthenticated) {
      this.props.history.push('/');
    }
  }
  
  handleSubmit = (values: any) => {
    this.props.loginFormRequest(values, this.props.history);
  }
  
  render() {
    return (
      <div>
        <LoadableLoginForm
          onSubmit={this.handleSubmit} 
          {...this.props}
        />
      </div>
    )
  }
}

const mapStateToProps = (state: any) => ({
  isRequest: state[moduleName].submit,
  isAuthenticated: state[moduleName].isAuthenticated
});

const mapDispatchToProps = ({
  loginFormRequest
});

export default connect<any, any>(mapStateToProps, mapDispatchToProps)(Login);
