import * as React from 'react';
import { connect } from 'react-redux';

// Local imports

import RegisterForm from 'src/containers/register';

import * as AuthDuck from 'src/ducks/auth';

class Register extends React.Component<any, any> {
  handleSubmit = (values: any) => {
    this.props.registerFormRequest(values, this.props.history);
  }

  render() {
    return (
      <div>
        <RegisterForm 
          onSubmit={this.handleSubmit}
          {...this.props}
        />        
      </div>
    )
  }
}

const mapStateToProps = (state: any) => ({
  isRequest: state[AuthDuck.moduleName].submit
});


const mapDispatchToProps = ({
  registerFormRequest: AuthDuck['registerFormRequest']
});

export default connect<{}, {}>(mapStateToProps, mapDispatchToProps)(Register);