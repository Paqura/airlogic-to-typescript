import * as React from 'react';
import { connect } from 'react-redux';

// Local imports

import * as HotelDuck from 'src/ducks/hotel';
import * as AuthDuck from 'src/ducks/auth';

import Hotel from 'src/containers/hotel';
import Loader from 'src/components/shared/Loader'; 

class HotelPage extends React.Component<any, any> {
  componentDidMount() {
    this.props.fetchOneHotel(this.props.match.params.id);
  }

  componentWillUnmount() {
    this.props.clearHotelState();
  }

  render() {
    const { hotelInfo, isAuthenticated } = this.props; 

    return hotelInfo ? 
      <Hotel 
        hotel={hotelInfo} 
        isAuthenticated={isAuthenticated}
        reserveHotelWithoutAuth={this.props.reserveHotelWithoutAuth}
      /> :
      <Loader.CloudLoader />
  }
};

const mapStateToProps = (state: any) => ({
  hotelInfo: state[HotelDuck.moduleName].info,
  isAuthenticated: state[AuthDuck.moduleName].isAuthenticated
});

const { fetchOneHotel, clearHotelState, reserveHotelWithoutAuth } = HotelDuck;

const mapDispatchToProps = ({
  fetchOneHotel,
  clearHotelState,
  reserveHotelWithoutAuth
});

export default connect(mapStateToProps, mapDispatchToProps)(HotelPage);