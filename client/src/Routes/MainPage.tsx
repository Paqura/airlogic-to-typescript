import * as React from 'react';
import {connect} from 'react-redux';

// Local imports

import { debounceFn } from 'src/utils/debounce';

import Header from 'src/components/shared/Header';
import HotelList from 'src/components/shared/HotelList';
import Footer from 'src/components/shared/Footer';

import * as HotelsDuck from 'src/ducks/hotels';
import * as AuthDuck from 'src/ducks/auth';
import * as SearchDuck from 'src/ducks/search';


class MainPage extends React.PureComponent<any, any> { 
  componentDidMount() {
    this.props.fetchHotelsRequest();
    window.addEventListener('scroll', this.debouncedScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.debouncedScroll, false);
  }
  
  debouncedScroll = () => {
    const cb = () =>  {
      return debounceFn(this.checkScroll, 120)();
    }
    return cb();
  }

  checkScroll = () => {
    const OFFSET = 140;
    const scrolled = Math.ceil(window.innerHeight + window.scrollY) + OFFSET;
    const maxHeightScroll = document.body.clientHeight;
    
    if(scrolled > maxHeightScroll) {
      !this.props.lazyLoading &&
      !this.props.loaded &&
      this.props.hotels.length &&
      this.props.fetchLazyHotelsRequest();
    } 
  }

  public render() {
    
    return (
      <div>
        <Header.MainPageHeader
          isAuthenticated={this.props.isAuthenticated} 
          find={this.props.findHotelsRequest}
          {...this.props}
        />
        <HotelList 
          hotels={ this.props.hotels && this.props.hotels } 
          lazyLoading={this.props.lazyLoading}
          title={'Hotels around the world'}
        />
        <Footer />
      </div>
    )
  }
}

const mapStateToProps = (state: any) => ({
  isAuthenticated: state[AuthDuck.moduleName].isAuthenticated,
  loaded: state[HotelsDuck.moduleName].loaded,
  lazyLoading: state[HotelsDuck.moduleName].lazyLoading,
  hotels: HotelsDuck.getHotelList(state[HotelsDuck.moduleName])
});

const { fetchHotelsRequest,  fetchLazyHotelsRequest } = HotelsDuck;
const { findHotelsRequest } = SearchDuck;

const mapDispatchToProps = ({
  fetchHotelsRequest,
  fetchLazyHotelsRequest,
  findHotelsRequest
});

export default connect<any, any>(mapStateToProps, mapDispatchToProps)(MainPage);
