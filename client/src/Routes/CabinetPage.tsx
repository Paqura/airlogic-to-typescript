import * as React from 'react';

// Local imports

import CabinetComponent from 'src/containers/cabinet';

class CabinetPage extends React.Component<any, any> {
  render() {
    return <CabinetComponent {...this.props}/>
  }
};

export default CabinetPage; 