import * as React from 'react';
import * as Loadable from 'react-loadable';
import { Switch, Route } from 'react-router';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

// Local imports

import { resetStateOfErrors } from 'src/ducks/errors';
import * as ErrorsDuck from 'src/ducks/errors';

import Toast from 'src/components/shared/Toast';
import LoginPage from './LoginPage';
import RegisterPage from './RegisterPage';
import MainPage from './MainPage';
import Loader from 'src/components/shared/Loader';
import CabinetPage from './CabinetPage';

// CODE PART

const LoadableHotel = Loadable({
  loader: () => import('./HotelPage'),
  loading: Loader.CircleLoader,
});

const LoadableSearch = Loadable({
  loader: () => import('./SearchPage'),
  loading: Loader.CloudLoader,
});

class Root extends React.Component<any, any> {  
  render() {   
    const {message} = this.props; 
    return (
      <div>
        <Switch>
          <Route exact={true}
            path="/" 
            render={(props) => <MainPage {...props} />} 
          />
          <Route exact={true}
            path="/search"
            render={(props) => <LoadableSearch {...props} />} 
          />
          <Route exact={true}
            path="/hotel/:id" 
            render={(props) => <LoadableHotel {...props} />}
          />
          <Route exact={true}
            path="/login"
            component={ LoginPage }
          />
          <Route exact={true}
            path="/register"
            component={ RegisterPage }
          />
          <Route
            path="/cabinet"
            render={(props) => <CabinetPage {...props} />}
          />
        </Switch>
        {message && 
          <Toast 
            message={message} 
            action={this.props.resetStateOfErrors}
        />}
      </div>
    )
  }
};

const mapStateToProps = (state: any) => ({
  message: state[ErrorsDuck.moduleName].message
});

const mapDispatchToProps = ({
  resetStateOfErrors
});

export default withRouter<any>(connect<any, any>(mapStateToProps, mapDispatchToProps)(Root));

