import * as React from 'react';
import { connect } from 'react-redux';

// Local imports

import Header from 'src/components/shared/Header';
import Search from 'src/containers/search'; 
import HotelList from 'src/components/shared/HotelList';

import * as AuthDuck from 'src/ducks/auth';
import * as SearchDuck from 'src/ducks/search';

class SearchPage extends React.Component<any, any> {
  render() {
    const { hotels, isAuthenticated } = this.props;
    return (
      <div>
        <Header.SharedHeader isAuthenticated={isAuthenticated} />
        <Search {...this.props} />
        <HotelList 
          lazyLoading={false}
          hotels={hotels || []}
          title={'Search result'}
        />
      </div>
    )
  }
}

const mapStateToProps = (state: any) => ({
  isAuthenticated: state[AuthDuck.moduleName].isAuthenticated,
  hotels: state[SearchDuck.moduleName].resultList,  
  errors: state[SearchDuck.moduleName].errors,
  findHotelsRequest: state[SearchDuck.moduleName].findHotelsRequest,
  isLoading: state[SearchDuck.moduleName].isLoading
});

const { findHotelsRequest } = SearchDuck;

const mapDispatchToProps = ({
  findHotelsRequest
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchPage);
