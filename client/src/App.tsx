import * as React from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import { HashRouter as Router } from 'react-router-dom';

declare global {
  interface Window {
    store: any;
  }
}

// Local imports

import history from 'src/history';
import store from 'src/store';

import { checkUserToken } from 'src/ducks/auth';

import Root from 'src/Routes/Root';

class App extends React.Component<{}, {}> {
  componentDidMount() {
    store.dispatch(checkUserToken());
  }

  public render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Router>
            <Root />
          </Router>
        </ConnectedRouter>
      </Provider>
    );
  }
}

window.store = store;

export default App;
