import * as React from 'react';
import * as ReactDOM from 'react-dom';

// Styles block

import './index.css';
import './vendors.css';

// Local imports

import App from './App';

import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
  <App />,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
