export interface IReserveHotelAction {
  type: 'RESERVE_HOTEL_REQUEST' | 'RESERVE_HOTEL_SUCCESS' | 'RESERVE_HOTEL_FAILURE';
  payload?: any;
}