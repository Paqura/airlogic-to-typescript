export interface ILoginFields {
  email: string;
  password: string;
};

export interface IRegisterFields extends ILoginFields {
  name: string;
};

export interface IAuthLoginAction {
  type: 'LOGIN_FORM_REQUEST',
  payload: {
    values: ILoginFields;
    history: any;
  }
};

export interface IAuthRegisterAction {
  type: 'REGISTER_FORM_REQUEST',
  payload: {
    values: IRegisterFields;
    history: any;
  }
};

