#Style Guide for create compoenents
---

1. Написание компонентов

```jsx
import библиотеки из node_modules

// SERVICES IMPORTS && DUCKS
// первым после библиотек идут сервисы

import * as AuthDuck from 'src/ducks/auth';

// COMPONENTS IMPORTS
// компоненты из проекта

import любые компоненты, не входящие в библиотеки

// STYLES IMPORTS
// стили, если это css in js

import * as Order from './style';

// PRECOMPONENT CODE
// код, который относится к компоненту,
// но находится вне его пределов(например инетрейсы)

export interface IOrder {
  _id: string;
  title: string;
  picture: string;
  price: number;  
};

// COMPONENT CODE
// сам компонент

function OrderComponent (props: IOrder) {
  const {_id, title, picture, price} = props;
  return (
    <Order.Root key={_id}>
      {title}
    </Order.Root>
  )
}

export default OrderComponent;

```

