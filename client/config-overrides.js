// @ts-ignore

const webpack = require('webpack');

module.exports = function override(config, env) {
  if(env === 'production') {
    config.plugins.push(
    
      new webpack.optimize.CommonsChunkPlugin({
        name: 'vendor',
        minChunks: function(module){
          return module.context && module.context.includes('node_modules');
        }
      }),
      new webpack.optimize.CommonsChunkPlugin({
        name: 'manifest',
        minChunks: Infinity
      })
      
    );
  }
 
  return config;
}