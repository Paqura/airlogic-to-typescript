const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const reserveSchema = new Schema({
  hotel: {
    type: Schema.Types.ObjectId,
    ref: 'hotel'
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  start: {
    type: Date,
    required: true
  },
  end: {
    type: Date,
    required: true
  }
});

module.exports = mongoose.model('reserve', reserveSchema);