const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const hotelSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  price: {
    type: String,
    required: true
  },
  picture: {
    type: String,
    required: true
  },
  rating: {
    type: String,
  },
  location: {
    lon: {
      type: String,
      required: false
    },
    lat: {
      type: String,
      required: false
    }
  },
  facilities: [
    {
      wifi: {
        type: Boolean
      },
      tv: {
        type: Boolean
      },
      parking: {
        type: Boolean
      },
      kitchen: {
        type: Boolean
      },
      bedsCount: {
        type: Number
      },
      reviews: {
        ref: 'reviews',
        type: Schema.Types.ObjectId
      }
    }
  ],
  photos: [String]
});

module.exports = mongoose.model('hotels', hotelSchema);