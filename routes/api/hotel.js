const express = require('express');
const router = express.Router();
const hotelController = require('../../controllers/hotel');

/**
 * @public
 */

router.get('/', hotelController.getAll);
router.get('/:hotel_id', hotelController.getById);
router.post('/', hotelController.find);

/**
 * @private
 */

router.post('/', hotelController.create);

module.exports = router;