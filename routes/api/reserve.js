const express = require('express');
const router = express.Router();
const reserveController = require('../../controllers/reserve');


/**
 * @public
 */

router.get('/', reserveController.getAll);
router.get('/:hotel_id', reserveController.getById);
router.get('/list/:user_id', reserveController.getListIds);
router.post('/', reserveController.addReserve);

module.exports = router;