const User = require('../models/User');
const Reserve = require('../models/Reserve');
const Hotel = require('../models/Hotel');
const keys = require('../config/keys');
const STATUS = require('../config/httpStatus');
const errorHandler = require('../utils/errorHandler');

module.exports.getAll = async function(req, res) {
  const hotels = await Hotel
    .find()
    .skip(+req.query.offset)
    .limit(+req.query.limit);
  
  if(!hotels) {
    console.log('Нет отелей');
  } else {
    res.status(STATUS.OK).json(hotels);
  }
}

module.exports.getById = async function(req, res) {
  let candidate = await Hotel.findById(req.params.hotel_id) || null;
  
  if(!candidate) {
    res.status(STATUS.NOT_FOUND).json({
      message: 'Отель не найден'
    })
  } else {
    res.status(STATUS.OK).json(candidate);
  } 
}

module.exports.find = async function (req, res) {
  const finder = req.body.name;

  const hotels = await Hotel.find({ title: new RegExp(finder, 'i', 'g') });
  
  if(hotels.length > 0) {
    res.status(STATUS.OK).json(hotels);
  } else {
    res.status(STATUS.NOT_FOUND).json({
      message: 'Ничего не найдено...'
    });
  }
}

module.exports.create = async function(req, res) {
  const newHotel = new Hotel({
    title: req.body.title,
    price: req.body.price,
    picture: req.body.picture,
    facilities: [
      ...req.body.facilities
    ]
  });

  await newHotel.save();
  res.send('create');
}