const moment = require('moment');

const Reserve = require('../models/Reserve');
const Hotel = require('../models/Hotel');
const STATUS = require('../config/httpStatus');
const MESSAGES = require('../config/messages');
const errorHandler = require('../utils/errorHandler');

module.exports.getAll = async function(req, res) { 
  Reserve.find().then(reserveHotels => {
    if(reserveHotels.length > 0) {
      res.status(STATUS.OK).json(reserveHotels);
    } else {
      res.status(STATUS.NOT_FOUND).json({
        message: 'Ничего не найдено'
      });
    }
  })  
}

module.exports.getById = async function(req, res) {
  Reserve.find({hotel: req.params.hotel_id})
    .then(reserve => {
      if(reserve) {
        const reserveFlag = Boolean(reserve[0]);
        res.status(STATUS.OK).json(reserveFlag);
      } else {
        res.status(STATUS.NOT_FOUND).json({
          message: 'Ничего не найдено'
        });
      }
    })
}

module.exports.getListIds = async function(req, res) {
  Reserve.find({user: req.params.user_id})
    .then(reserve => {
      if(reserve) {
        const ids = reserve.map(reserve => reserve.hotel);
        
        Hotel.find({ '_id': { $in: ids } })
          .then(hotels => {
            res.status(STATUS.OK).json(hotels);
          })
          .catch(err => res.status(STATUS.NOT_FOUND).json(err))

      } else {
        res.status(STATUS.NOT_FOUND).json({
          message: 'Ничего не найдено'
        });
      }
    })
}

module.exports.addReserve = async function(req, res) {
  const candidate = new Reserve ({
    start: req.body.startDate,
    end: req.body.endDate,
    hotel: req.body.hotelId,
    user: req.body.userId
  });

  Reserve.find({hotel: candidate.hotel})
    .then(reserve => {
      if(reserve.length === 0) {
        candidate.save().then(reserve => {
          res.status(STATUS.OK).json({ message: 'Done'})
        });
      } else {
        res.status(STATUS.CONFLICT).json({
          message: 'The hotel is busy'
        })
      }
    })
    .catch(err => {
      res.status(STATUS.CONFLICT).json({
        message: 'The hotel is busy'
      })
    })
}